#!/usr/bin/perl
##Melanie 31.05.2013
#Usage: SortByNr.pl simReads.file

use strict; use warnings; use diagnostics;

my $file = $ARGV[0];
open(FILE, $file) or die "Can't open $file.\n";

my $line=0;
my $count=0;

while($line=<FILE>){
	if($line=~/^>/){
		$count++;
	}
}

my @singleRead = (0,0);
my @reads = (\@singleRead);

my $i=1;

for ($i=1;$i<$count;$i++){
	my @singleRead = (0,0);
	push @reads, \@singleRead;

}


my $line1=0;
my $nr=0;

seek FILE, 0, 0;

while($line1=<FILE>){
        if($line1=~/^>/){
         	$line1=~/_nr(\d+)/;
		$nr=$1;
		$reads[$nr][0]=$line1;
		$line1=<FILE>;
		$reads[$nr][1]=$line1;
        }
}


for($i=0;$i<$count;$i++){
	print "$reads[$i][0]";
	print "$reads[$i][1]";
}

close(FILE);

