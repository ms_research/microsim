Author: Melanie Schirmer, The Broad Institute, mail@melanieschirmer.com

#######################
###This program is designed to simulates next-generation sequencing reads 
###from a set of input sequences/genomes. You can specify the frequency 
###distribution of the input sequences or use a uniform distribution 
###(all genomes occur with the same abundance). Furthermore you can choose 
###to simulate the reads with or without noise.
#######################



#####
##Dependencies
####

GSL - GNU Scientific Library (random number generator)

    To check if GSL is already installed use the command "info gsl". Otherwise you can download it 
    from "http://www.gnu.org/software/gsl/".

    If you place the files "gsl_rng.h" and "gsl_randist.h" in a different location than 
    "/usr/include/gsl/" then you have to change the path in the "opemMP_makefile" file for the 
    -I option of the CFLAGS accordingly before the installation.


OpenMP (for parallelisation)


The program has been tested on Linux (gcc version 4.4.6). (Note that the current version of Xcode 
(version 6) for Mac OS does not fully support openMP.)


#####
##Installation
#####

To install the program run the following command within the directory MicroSim:

    make -f openMP_makefile


#####
##Usage
#####

Copy the file "ParInput_parallel_v0.1.txt" and adjust the parameter according to your 
data set. Then run:

    ./MicroSim_v0.1_parallel ParInput_parallel_v0.1.txt

(The files currently specified in the parameter file are supplied with the program and 
located in the directory "Data".)

If you are simulating paired-end reads using multiple threads, you need to sort the 
R1 and R2. (This is not necessary if the simulation was run on a single CPU/thread.)  
Run the following perl script once the read simulation is completed 
(to sort by read number):

    SortByNr.pl R1_seedxx_CPUsxx.fasta > R1_seedxx_CPUsxx_sorted.fasta
    SortByNr.pl R2_seedxx_CPUsxx.fasta > R2_seedxx_CPUsxx_sorted.fasta



###############
#### CITATION 
################

Melanie Schirmer, William T. Sloan and Christopher Quince. MicroSim: A motif-based 
next-generation read simulator. [In preparation].


################
#### SUPPORT
################

If you would like to report a bug, please contact: mail@melanieschirmer.com



