//#Last update 31.01.2015
//#Author: Melanie Schirmer, The Broad Institute, mail@melanieschirmer.com

//###########################
//#License: FreeBSD
//#
//#Copyright (c) 2015, Melanie Schirmer.
//#
//#All rights reserved.
//#
//#Redistribution and use in source and binary forms, with or without
//#modification, are permitted provided that the following conditions are met:
//#
//#1. Redistributions of source code must retain the above copyright notice, this
//#   list of conditions and the following disclaimer.
//#2. Redistributions in binary form must reproduce the above copyright notice,
//#   this list of conditions and the following disclaimer in the documentation
//#   and/or other materials provided with the distribution.
//#
//#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//#
//#The views and conclusions contained in the software and documentation are those
//#of the authors and should not be interpreted as representing official policies,
//#either expressed or implied, of the FreeBSD Project.
//#
//#########################



#define DELIM " "
#define DELIM2 "\n"
#define DELIM3 "'"
#define DELIM4 ","
#define DELIM5 "\t"
#define MAX_NAME_LEN 500
#define MAXFILENAME 500
#define eps 0.00000000000000001
#define UPPER_LIM_INS 20	//upper limit for the number of deletions occuring on one read to ensure the starting position is far enough form the end of the genome
#define UPPER_LIM_DEL 20
#define LargeNr 1000000
#define MAX_ITER 300		//max number of iterations to find starting position of read



typedef struct hap_set {			
	char** hap_name;
	char** hap_seq;
} HAP_SET;


// data type "insertDist"
typedef struct inD {			
	int* freq;
	int* len;
} inD;


// data type "noise_line"
typedef struct noise_line{			
	double** qual_score;
	double** freq;
} NOISE_LINE;




//read-in insert size distribution
inD* loadInsertDist(inD *InDist, char *szInsertDistFile)		
{	
	FILE *ifp = NULL;
	ifp = fopen(szInsertDistFile,"r");
	char eachLine[2*sizeof(int)+sizeof("\t")+sizeof("\n")];				//stores each line
	char *szTokNoise = NULL;
	int counter=-1;
	
	if(ifp){											
		while(fgets(eachLine, (2*sizeof(int)+sizeof("\t")+sizeof("\n")), ifp) != NULL){
			counter++;
			szTokNoise=strtok(eachLine,DELIM5);
			*(InDist[counter].freq) = atoi(szTokNoise);	
			szTokNoise=strtok(NULL,DELIM5);
			*(InDist[counter].len) = atoi(szTokNoise);	
		}		
		fclose(ifp);
	}
	else{
		// code for file open failure
		fprintf(stderr, "Failed to open file insert distribution file.\n");
        exit(0);
	}
	printf("Done with insert size distribution ...\n");	
	return(InDist);
}




//determine next reference sequence
int findHap(double randX, double *frequencies, int NR_GENOMES)
{
	double sum_freq = 0;
	int nextHap = -1;
	int i=0;
	
	for(i=0;i<NR_GENOMES;i++){
		
		if(sum_freq < 1 && frequencies[i]!=0){
			if( (randX >= sum_freq) && (randX < (sum_freq + frequencies[i]))){
				nextHap = i;
				break;
			}
			else{
				sum_freq = sum_freq + frequencies[i];
			}
		}
	}
		
	if(nextHap==-1){
        nextHap = NR_GENOMES-1;
		//printf("Problem with finding next sequence.\n");
		//exit(0);
	}
	
	return nextHap;
}


void loadIllNucOcc(FILE *ifp_illumina_nucOcc, int ILL_L,int** nucATGC)
{
	int IllBase=-1;
	char eachNuc[100];
	char *szTokNuc = NULL;
	int i=0;
	
	if(ifp_illumina_nucOcc){
			
		for(i=0;i<(4*ILL_L);i++){
			if(i<ILL_L){
				IllBase=0;		//corresponds to A
			}
			else{
				if(i<(2*ILL_L) && i>(ILL_L-1)){
					IllBase=1;		//corresponds to T
				}
				else{
					if(i<(3*ILL_L) && i>((2*ILL_L)-1)){
						IllBase=2;		//corresponds to G
					}
					else{
						if(i<(4*ILL_L) && i>((3*ILL_L)-1)){
							IllBase=3;		//corresponds to C
						}
						else{
							printf("Problem with Illumina nucleotides occurrence profile.\n\n");
							exit(0);
						}
					}
				}
			}
			
			//read nucleotide occurances
			fgets(eachNuc, ILL_L, ifp_illumina_nucOcc);
			//printf("fwd nuc=%s\n", eachNuc_fwd);
			szTokNuc = strtok(eachNuc,DELIM2);
			nucATGC[IllBase][i % ILL_L]=atoi(szTokNuc);
			
		}
		fclose(ifp_illumina_nucOcc);
	}
	else{
		printf("Failed to open Illumina noise file.\n");
		exit(0);
	}
	
}




void loadIllNoise(FILE *ifp_illumina_noise, int ILL_L, double*** Ill_Noise, int** nucATGC)
{
	int IllBase=-1;
	char eachIllNoise[1000];
	char *szTokNoise = NULL;
	int indexNoise=0;
	int i=0;
	
	if(ifp_illumina_noise){
		
		for(i=0;i<(4*ILL_L);i++){
			if(i<ILL_L){
				IllBase=0;		//corresponds to A
			}
			else{
				if(i<(2*ILL_L) && i>(ILL_L-1)){
					IllBase=1;		//corresponds to T
				}
				else{
					if(i<(3*ILL_L) && i>((2*ILL_L)-1)){
						IllBase=2;		//corresponds to G
					}
					else{
						if(i<(4*ILL_L) && i>((3*ILL_L)-1)){
							IllBase=3;		//corresponds to C
						}
						else{
							printf("Problem Illumina 100bp noise profile.\n\n");
							exit(0);
						}
					}
				}
			}
		
			//forward/reverse read noise profile
			fgets(eachIllNoise, ILL_L, ifp_illumina_noise);
			szTokNoise = strtok(eachIllNoise,DELIM5);
			
			indexNoise=0;
			while (indexNoise<4) {
				//For Amplicon error profiles the number of a specific nucleotide can be zero
				if((nucATGC[IllBase][i % ILL_L] - eps)<0 && (atof(szTokNoise) - eps)<0){
					Ill_Noise[IllBase][i % ILL_L][indexNoise] = 0;
				}
				else{	
					Ill_Noise[IllBase][i % ILL_L][indexNoise] = atof(szTokNoise)/nucATGC[IllBase][i % ILL_L];
				}
				szTokNoise = strtok(NULL, DELIM5);
				indexNoise++;
			}
		
		}
		fclose(ifp_illumina_noise);
	}
	else{
		printf("Failed to open Illumina noise file.\n");
	}
	
}


void loadIllIndel(FILE *ifp_illumina_del, int ILL_L, double** Ill_Noise_Del_for,int** nucATGC){
	
	char eachIllNoise[1000];
	char *szTokNoise = NULL;
	int indexNoise=0;
	int i=0;
	
	if(ifp_illumina_del){
		for(i=0;i<ILL_L;i++){
			
			//forward read deletions
			fgets(eachIllNoise, ILL_L, ifp_illumina_del);
			szTokNoise = strtok(eachIllNoise,DELIM5);
			
			indexNoise=0;
			while (indexNoise<4) {
				if((Ill_Noise_Del_for[i][indexNoise] - eps)<0 && (atof(szTokNoise) - eps)<0){
					Ill_Noise_Del_for[i][indexNoise] = 0;
				}
				else{	
					Ill_Noise_Del_for[i][indexNoise] = atof(szTokNoise)/(nucATGC[0][i]+nucATGC[1][i]+nucATGC[2][i]+nucATGC[3][i]);
				}
				szTokNoise = strtok(NULL, DELIM5);
				indexNoise++;
			}
		}
		fclose(ifp_illumina_del);
	}
	else{
		printf("Failed to open Illumina noise file (del).\n");
	}
}




//Motif based noise 
void loadMotifOcc(FILE *ifp_illumina_noise, int** Ill_motif)
{
	char eachIllNoise[1000];
	char *szTokNoise = NULL;
	int i=0;
	
	if(ifp_illumina_noise){
		
		for(i=0;i<64;i++){
			fgets(eachIllNoise, 100, ifp_illumina_noise);
			//A
			szTokNoise = strtok(eachIllNoise,DELIM5);
			Ill_motif[i][0]=atof(szTokNoise);
			//C
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][1]=atof(szTokNoise);
			//G
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][2]=atof(szTokNoise);
			//T
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][3]=atof(szTokNoise);
			
			
		}
		fclose(ifp_illumina_noise);
	}
	else{
		printf("Failed to open Illumina noise file.\n");
	}
	
}

void loadMotif(FILE *ifp_illumina_noise, double** Ill_motif, int** nucATGC_motif)
{
	char eachIllNoise[1000];
	char *szTokNoise = NULL;
	int i=0;
	
	if(ifp_illumina_noise){

		for(i=0;i<64;i++){
			fgets(eachIllNoise, 100, ifp_illumina_noise);
			//motif
			szTokNoise = strtok(eachIllNoise,DELIM5);
			//A
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][0]=atof(szTokNoise)/nucATGC_motif[i][0];
			//C
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][1]=atof(szTokNoise)/nucATGC_motif[i][1];
			//G
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][2]=atof(szTokNoise)/nucATGC_motif[i][2];
			//T
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][3]=atof(szTokNoise)/nucATGC_motif[i][3];
			
			
		}
		fclose(ifp_illumina_noise);
	}
	else{
		printf("Failed to open Illumina noise file.\n");
	}
	
}



void loadMotifIns(FILE *ifp_illumina_noise, double** Ill_motif, int** nucATGC_motif)
{
	char eachIllNoise[1000];
	char *szTokNoise = NULL;
	int i=0;
	
	if(ifp_illumina_noise){
		
		for(i=0;i<64;i++){
			fgets(eachIllNoise, 100, ifp_illumina_noise);
			//motif
			szTokNoise = strtok(eachIllNoise,DELIM5);
			//A
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][0]=atof(szTokNoise)/(nucATGC_motif[i][0]+nucATGC_motif[i][1]+nucATGC_motif[i][2]+nucATGC_motif[i][3]);
			//C
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][1]=atof(szTokNoise)/(nucATGC_motif[i][0]+nucATGC_motif[i][1]+nucATGC_motif[i][2]+nucATGC_motif[i][3]);
			//G
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][2]=atof(szTokNoise)/(nucATGC_motif[i][0]+nucATGC_motif[i][1]+nucATGC_motif[i][2]+nucATGC_motif[i][3]);
			//T
			szTokNoise = strtok(NULL,DELIM5);
			Ill_motif[i][3]=atof(szTokNoise)/(nucATGC_motif[i][0]+nucATGC_motif[i][1]+nucATGC_motif[i][2]+nucATGC_motif[i][3]);
			
			
		}
		fclose(ifp_illumina_noise);
	}
	else{
		printf("Failed to open Illumina noise file.\n");
	}
	
}



double** loadNoise(double** dist, int n)
{
	FILE *ifp_noise = NULL;
	ifp_noise = fopen("Noise/LookUp_Titanium.dat","r");
	
	if(ifp_noise){
		int i=0;
		char eachNoise[100];
		double sum=0;
		
		for(i=0;i<(n*1001)+1;i++){
			fgets(eachNoise, 100, ifp_noise);
		}
		
		for(i=0;i<1000;i++){
		
			fgets(eachNoise, 100, ifp_noise);
			sum=sum+ exp(-atof(eachNoise));
			
			dist[i][0] = sum;
		}
		
		fclose(ifp_noise);
	}
	else{
		fprintf(stderr, "Failed to open noise file.\n");
		exit(0);			
	}
	return dist;
}







double addNoise(double randNoise, double** dist)
{
	double noisyBase=-1;
	int noisyPos=-1;
	int i=0;
	
    
    	for(i=0;i<1000;i++){
        //printf("i=%d\n",i);
		if(randNoise>dist[i][0]){
		}
		else{
			noisyPos=i;
			break;
		}
        if(i==1000){
            noisyPos=1000;
        }
	}

    
    
	noisyBase=(double)noisyPos/100;
		
	return noisyBase;
}




char addPCRNoise(char nucleo, double nuc_change_prob, double nucA[4], double nucC[4], double nucG[4], double nucT[4])
{
	switch (nucleo) {
		case 'A':
		case 'a':
			//draw a random number between 0 and 1
			if(nuc_change_prob < nucA[0]){
				nucleo = 'A';
				//printf("A -> A \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= nucA[0] && nuc_change_prob < (nucA[0] + nucA[1])){
				nucleo = 'C';
				//printf("A -> C \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= (nucA[0] + nucA[1]) && nuc_change_prob < (nucA[0] + nucA[1] + nucA[2])){
				nucleo = 'T';
				//printf("A -> T \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= (nucA[0] + nucA[1] + nucA[2]) && nuc_change_prob < (nucA[0] + nucA[1] + nucA[2] + nucA[3])){
				nucleo = 'G';
				//printf("A -> G \t %c \n",nucleo);
				break;
			}
			else{
				printf("problem with A\n");
				printf("nuc_change_prob=%f\n",nuc_change_prob);
				exit(0);
			}
			
		case 'C':
		case 'c':
			
			if(nuc_change_prob < nucC[0]){
				nucleo = 'A';
				//printf("C -> A \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= nucC[0] && nuc_change_prob < (nucC[0] + nucC[1])){
				nucleo = 'C';
				//printf("C -> C \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= (nucC[0] + nucC[1]) && nuc_change_prob < (nucC[0] + nucC[1] + nucC[2])){
				nucleo = 'T';
				//printf("C -> T \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= (nucC[0] + nucC[1] + nucC[2]) && nuc_change_prob < (nucC[0] + nucC[1] + nucC[2] + nucC[3])){
				nucleo = 'G';
				//printf("C -> G \t %c \n",nucleo);
				break;
			}
			else{
				printf("problem with C\n");
				printf("nuc_change_prob=%f\n",nuc_change_prob);
				exit(0);
			}
			
			
		case 'T':
		case 't':
			
			if(nuc_change_prob < nucT[0]){
				nucleo = 'A';
				//printf("T -> A \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= nucT[0] && nuc_change_prob < (nucT[0] + nucT[1])){
				nucleo = 'C';
				//printf("T -> C \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= (nucT[0] + nucT[1]) && nuc_change_prob < (nucT[0] + nucT[1] + nucT[2])){
				nucleo = 'T';
				//printf("T -> T \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= (nucT[0] + nucT[1] + nucT[2]) && nuc_change_prob < (nucT[0] + nucT[1] + nucT[2] + nucT[3])){
				nucleo = 'G';
				//printf("T -> G \t %c \n",nucleo);
				break;
			}
			else{
				printf("problem with T\n");
				printf("nuc_change_prob=%f\n",nuc_change_prob);
				exit(0);
			}
			
			
		case 'G':
		case 'g':
			
			if(nuc_change_prob < nucG[0]){
				nucleo = 'A';
				//printf("G -> A \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= nucG[0] && nuc_change_prob < (nucG[0] + nucG[1])){
				nucleo = 'C';
				//printf("G -> C \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= (nucG[0] + nucG[1]) && nuc_change_prob < (nucG[0] + nucG[1] + nucG[2])){
				nucleo = 'T';
				//printf("G -> T \t %c \n",nucleo);
				break;
			}
			
			if(nuc_change_prob >= (nucG[0] + nucG[1] + nucG[2]) && nuc_change_prob < (nucG[0] + nucG[1] + nucG[2] + nucG[3])){
				nucleo = 'G';
				//printf("G -> G \t %c \n",nucleo);
				break;
			}
			else{
				printf("problem with G\n");
				printf("nuc_change_prob=%f\n",nuc_change_prob);
				exit(0);
			}
			
		default:
			printf("Problem function addPCRNoise.\n");
			printf("nucleo=%c\n",nucleo);
			exit(0);
			break;
	}
	
	return nucleo;
} 



//print complementary nucleotide
int complementNuc(char Nuc){
	
	char compNuc;
	switch(Nuc){
		case 'A':
			compNuc='T';
			break;
		case 'T':
			compNuc='A';
			break;
		case 'G':
			compNuc='C';
			break;
		case 'C':
			compNuc='G';
			break;
		case 'a':
			compNuc='t';
			break;
		case 't':
			compNuc='a';
			break;
		case 'g':
			compNuc='c';
			break;
		case 'c':
			compNuc='g';
			break;
		default:
			printf("Problem with function complemtnNuc.\n%c\n",Nuc);
            exit(0);
			break;
	}
	
	return compNuc;
}




int nucTOint(char nuc)
{
	int Nuc100 = -1;
	
	//nucleotides: 0->"A",1->"T",2->"G",3->"C"
	switch (nuc) {
		case 'A':
		case 'a':
			Nuc100=0;
			break;
		case 'T':
		case 't':
			Nuc100=1;
			break;
		case 'G':
		case 'g':
			Nuc100=2;
			break;
		case 'C':
		case 'c':
			Nuc100=3;
			break;
		default:
			break;
	}
	
	return Nuc100;
}


int nucTOint2(char nuc)
{
	int Nuc100 = -1;
	
	//nucleotides: 0->"A",1->"T",2->"G",3->"C"
	switch (nuc) {
		case 'A':
		case 'a':
			Nuc100=0;
			break;
		case 'T':
		case 't':
			Nuc100=3;
			break;
		case 'G':
		case 'g':
			Nuc100=2;
			break;
		case 'C':
		case 'c':
			Nuc100=1;
			break;
		default:
			break;
	}
	
	return Nuc100;
}


