//Last update 31.01.2015
//Author: Melanie Schirmer, The Broad Institute, mail@melanieschirmer.com

//###########################
//#License: FreeBSD
//#
//#Copyright (c) 2015, Melanie Schirmer.
//#
//#All rights reserved.
//#
//#Redistribution and use in source and binary forms, with or without
//#modification, are permitted provided that the following conditions are met: 
//#
//#1. Redistributions of source code must retain the above copyright notice, this
//#   list of conditions and the following disclaimer. 
//#2. Redistributions in binary form must reproduce the above copyright notice,
//#   this list of conditions and the following disclaimer in the documentation
//#   and/or other materials provided with the distribution. 
//#
//#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//#
//#The views and conclusions contained in the software and documentation are those
//#of the authors and should not be interpreted as representing official policies, 
//#either expressed or implied, of the FreeBSD Project.
//#
//#########################


//$valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes ./command

/* system includes*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "MicroSim_v0.1_parallel.h"
#include <time.h>
#include <gsl/gsl_rng.h>		
#include <gsl/gsl_randist.h>
#include <zlib.h>		
#include "kseq.h"		
#include <unistd.h>

#ifdef _OPENMP
#include <omp.h>
#endif

KSEQ_INIT(gzFile, gzread)

///////////////
// main function
///////////////


int main(int argc, char* argv[])
{
	int position=-1;
	int i=0,j=0,k=0,z=0;
	int checkDig=1;
	double randNoise=0;
	char* std_char = (char*)malloc(sizeof(char));		
	strncpy(std_char,"o",1);			
	double noiseCount=0;
	char* FASTQ = (char*)malloc(5*sizeof(char));
	strncpy(FASTQ,"fastq",5);
	int pcrError=0, SeqError=0;
	int insertSize_freq=0, insertSize=0;
	int c_ins=0, sum_ins=0;
	int Nuc100=-1;
	int Nuc100_2=-1;
	double sum100=0;
	int checkReadLen=0, checkReadLen_R=0;
	int pickHap =0;		
	double randX=0;
	int LENGTH=0;
	double ins_sum=0;
	double ins_sum_rev=0;
	int motif=-1;
	
	/////////////////////////
	//Read in the parameters 
	/////////////////////////
	
	//"ParInput.txt"
	char *szParFile = (char*)malloc(200*sizeof(char));
	
	if(argc==2){
		strcpy(szParFile,argv[1]);
		printf("\n\n\nParameter file: %s\n",szParFile);
				
	}
	else{
		printf("Wrong number of arguments!\n");
		exit(0);
	}
	
	
	int ParCount = 0;
	FILE *ifp_par = NULL;
	int MAX_LEN_Par=200;
	char eachPar[MAX_LEN_Par];
	char *szHapFile = NULL;
	char *szFreqFile = NULL;
	char *szReadsFormat = NULL;
	char *SampleName = NULL;
	char tempHap[200];
	char tempHap2[200];
	char tempHap3[200];
	char tempHap4[200];
	char temp[200];
	char FLOW_ORDER[4];
	char FLOW_ORDER_2[4]; //to cover lower and upper case letters
	
	for(i=0;i<200;i++){
		tempHap[i]=0;
		tempHap2[i]=0;
		tempHap3[i]=0;
		tempHap4[i]=0;
		temp[i]=0;
	}
	for(i=0;i<4;i++){
		FLOW_ORDER[i]=0;
		FLOW_ORDER_2[i]=0;
	}
	
	int nrThreads = 0;
	int NR_GENOMES=0;
	int LEN_GENOMES=0,HAPFREQ=-1,CYCLE_NR=800,Total_Nr_Reads=0,Noise=0,PCR_NOISE=0,STANDARD_DEVIATION=0,MYSEED=0,MYAMPLICON=-1, MYPAIREDEND=-1,ILL_PROFILE=-1,READ_LENGTH=-1,INSERT_D=-1;

	ifp_par = fopen(szParFile,"r");
	
	if(ifp_par){
		
		printf("\n\n--------------------------------------\n\nRead parameters from file %s.\n",szParFile);
	
		while(fgets(eachPar, MAX_LEN_Par, ifp_par) != NULL){
		
			if(!strncmp(eachPar,DELIM3,1)){
				
				switch (ParCount){
					case 0: 
						strcpy(tempHap,strtok(eachPar,DELIM3));
						szHapFile=tempHap;
						printf("Read reference sequences from file %s.\n",szHapFile);
						ParCount=1;		
						break;
				
					case 1:
						strcpy(tempHap2,strtok(eachPar,DELIM3));
						HAPFREQ=atoi(tempHap2);
						if(HAPFREQ==0){
							printf("The abundances follow a uniform abundance distribution.\n");
						}
						else{
							if(HAPFREQ==1){
								printf("The abundance distribution is non-uniform.\n");
							}
							else{
								printf("Problem with specifying an abundance distribution.\n");
								exit(0);
							}
						}
						ParCount=2;		
						break;
						
					case 2:
						if(HAPFREQ==0){
							//nothing to read in if uniform abundance is chosen
							ParCount=3;
							break;
						}
						if(HAPFREQ==1){
							//read frequencies from file
							strcpy(tempHap3,strtok(eachPar,DELIM3));
							szFreqFile=tempHap3;
							printf("Read frequencies from file %s.\n",szFreqFile);
							ParCount=3;
							break;
						}
					
					case 3:
						strcpy(tempHap2,strtok(eachPar,DELIM3));
						Total_Nr_Reads=atoi(tempHap2);
						printf("Number of reads that will be simulated: %d\n",Total_Nr_Reads);
						ParCount=4;
						break;
						
					case 4:
						strcpy(tempHap2,strtok(eachPar,DELIM3));
						Noise=atoi(tempHap2);
						switch (Noise) {
							case 0:
								printf("No noise is added to the reads.\n");
								break;
							//case 1:
								//printf("Reads contain 454 Titanium noise.\n");
								//break;
							case 2:
								printf("Reads contain Illumina noise.\n");
								break;
							default:
								printf("Problem with choice about adding noise to reads.\n");
                                exit(0);
								break;
						}
						ParCount=5;
						break;

					case 5:
						strcpy(tempHap2,strtok(eachPar,DELIM3));
						READ_LENGTH=atoi(strtok(tempHap2,DELIM4));
						STANDARD_DEVIATION=atoi(strtok(NULL,DELIM4));
						if(Noise==0){
							printf("Mean read length: %d\nStandard deviation:%d\n",READ_LENGTH,STANDARD_DEVIATION);
						}
						ParCount=8;
						break;
					//
					//case 6:
						//strcpy(tempHap2,strtok(eachPar,DELIM3));
						//CYCLE_NR= atoi(strtok(tempHap2,DELIM4));
                        //strncpy(FLOW_ORDER,strtok(NULL,DELIM4),4);
                        //for(k=0;k<4;k++){
						//	if(FLOW_ORDER[k]>=97&&FLOW_ORDER[k]<=122){
						//		FLOW_ORDER_2[k]=FLOW_ORDER[k]-32;
						//	}
						//	else{
						//		FLOW_ORDER_2[k]=FLOW_ORDER[k]+32;
						//	}
						//}
						//if(Noise==1){
						//	printf("Number of cycles for 454 reads: %d.\tOrder of nucleotides: %c%c%c%c\n",CYCLE_NR,FLOW_ORDER[0],FLOW_ORDER[1],FLOW_ORDER[2],FLOW_ORDER[3]);
						//}
						//ParCount=7;
						//break;
					//
					//case 7:
					//	if(Noise==1){
					//		strcpy(tempHap2,strtok(eachPar,DELIM3));
					//		PCR_NOISE=atoi(tempHap2);
					//		if(PCR_NOISE==0){
					//			printf("No PCR noise is added to reads.\n");
					//		}
					//		else{
					//			if(PCR_NOISE==1){
					//				printf("Reads contain PCR noise.\n");
					//			}
					//			else{
					//				printf("Problem with choice about adding PCR noise to reads.\n");
					//			}
					//		}
					//	}
					//	ParCount=8;
					//	break;
						
					case 8:
						strcpy(tempHap2,strtok(eachPar,DELIM3));
                        MYAMPLICON=atoi(tempHap2);
                        printf("WGS '0' or Amplicons '1': %d\n",MYAMPLICON);
						ParCount=9;
						break;
						
					case 9:
						strcpy(tempHap2,strtok(eachPar,DELIM3));
						INSERT_D=atoi(tempHap2);
                        if(MYAMPLICON==0){
                            if(INSERT_D==1 || INSERT_D==2 || INSERT_D==3){
                                printf("Insert size distribution %d\n",INSERT_D);
                            }
                            else{
                                INSERT_D=1;
                                printf("Insert size distribution %d\n",INSERT_D);
                            }
                        }
						ParCount=10;
						break;
						
						
					case 10:
                        if(Noise==2){
                            strcpy(tempHap2,strtok(eachPar,DELIM3));
                            ILL_PROFILE=atoi(tempHap2);
                            if(Noise==2){
                                printf("Illumina error profile: %d\n",ILL_PROFILE);
                            }
                        }
                        else{
                            ILL_PROFILE=100;
                        }
                        ParCount=11;
						break;	
						
					case 11:
						strcpy(tempHap2,strtok(eachPar,DELIM3));
						MYSEED=atoi(tempHap2);
						printf("Seed: %d\n",MYSEED);
						ParCount=12;
						break;		
						
					case 12:
						strcpy(tempHap2,strtok(eachPar,DELIM3));
						MYPAIREDEND=atoi(tempHap2);
                        if(Noise==1){
                            MYPAIREDEND=0;
                        }
						printf("Single reads '0' or paired-end reads '1': %d\n",MYPAIREDEND);
						ParCount=13;
						break;		
						
					case 13:
						strcpy(temp,strtok(eachPar,DELIM3));
						nrThreads=atoi(temp);
						printf("Number of CPUs/threads for parallel computations: %d\n",nrThreads);
						printf("\n--------------------------------------\n\n");
						ParCount=14;
						break;		
						
					case 14:
						strcpy(tempHap4,strtok(eachPar,DELIM3));
						SampleName=tempHap4;
						ParCount=15;
						break;
				}
			}
		}
		
		printf("\n\nDone with reading parameter file ...\n\n");
		fclose(ifp_par);
	}
	else{
		fprintf(stderr, "Failed to open file %s\n",szParFile);		// stderr		exit(0);
	}
	
    
    if(MYPAIREDEND==1){
        if(Noise==0){
            printf("-----------ENSURE YOUR CHOICE OF READ LENGTH IS SUITABLE FOR THE CHOSEN INSERT SIZE DISTRIBUTION----------\n\n");
        }
    }
	
	if(MYPAIREDEND==1 && Noise==1){
		printf("There is no paired-end option for 454 reads. Output will be single reads.\n\n");
		MYPAIREDEND=0;
	}
	
	
	/////////////////////////
	//Read in data
	/////////////////////////

	//---->read in reference sequences
	
	//number of sequences
	int nr_hap = 0;			

	gzFile fp;
	kseq_t *seq;
	int l;
	fp = gzopen(szHapFile, "r");

	if(fp){
		seq = kseq_init(fp);
		while ((l = kseq_read(seq)) >= 0) {
			//printf("name: %s\n", seq->name.s);
			//printf("seq: %s\n", seq->seq.s);
			nr_hap++;
		}
		kseq_destroy(seq);
		gzclose(fp);
	}
	
	printf("----------> Number of reference sequences = %d\n",nr_hap);
	
		
	//length of sequences
	int** len_hap = (int**) malloc(nr_hap*sizeof(int*));	
	if(len_hap){
		for(i=0;i<nr_hap;i++){
			len_hap[i] = (int*)malloc(sizeof(int));
			*len_hap[i] = 0;
		}
	}
	else{
		printf("Problem with memory allocation.\n");
		exit(0);
	}
	
	fp = gzopen(szHapFile, "r");
	if(fp){
		seq = kseq_init(fp);

		i=0;
		while ((l = kseq_read(seq)) >= 0) {	
			*len_hap[i]=seq->seq.l-1;		//length of nucleotide sequence
			i++;
		}  
		kseq_destroy(seq);			//destroy seq 
		gzclose(fp);
	}
	
	
	
	//allocate memory and initilise hap_set variable for sequences
	HAP_SET* haplos = (HAP_SET*) malloc(sizeof(HAP_SET));
	
	haplos->hap_name = (char **) malloc(nr_hap*sizeof(char*));
	//printf("Allocate space for hap_name...\n\n");
	if(haplos->hap_name){
		for(i=0;i<nr_hap;i++){
			
			haplos->hap_name[i]= (char*) malloc(MAX_NAME_LEN*sizeof(char));
			
			if(!haplos->hap_name){				
				printf("problem with memory allocation\n\n");
			}
			
			for(k=0; k<MAX_NAME_LEN; k++){
				haplos->hap_name[i][k]=*std_char;			//initialise polymorphisms to be -1 (-> no polymorphism)
			}
			//printf("%s\n",haplos->hap_name[i]);
		}
	}
	
	haplos->hap_seq = (char **) malloc(nr_hap*sizeof(char*));
	//printf("Allocate space for hap_seq...\n\n");
	if(haplos->hap_seq){
		for(i=0;i<nr_hap;i++){
			haplos->hap_seq[i]= (char*) malloc((*len_hap[i]+1)*sizeof(char));
			
			if(!haplos->hap_seq){				
				printf("problem with memory allocation\n\n");
			}
			
			for(k=0; k<*len_hap[i]+1; k++){
				haplos->hap_seq[i][k] = *std_char;			
			}
			//printf("%s\n",haplos->hap_seq[i]);
		}
	}
	
	
	fp = gzopen(szHapFile, "r");	//open the file handler  
	if(fp){
		//initialize seq
		seq = kseq_init(fp);			  
    
 		i=0;
		while ((l = kseq_read(seq)) >= 0) {			//read sequence  
			//printf("%s\n",seq->name.s);
			strcpy(haplos->hap_name[i],seq->name.s);
			z=0;
			for(j=0;j<*len_hap[i]+1;j++){
				if((char)(seq->seq.s[j])== 'A' || (char)(seq->seq.s[j])=='T' || (char)(seq->seq.s[j])=='G' || (char)(seq->seq.s[j])=='C' || (char)(seq->seq.s[j])== 'a' || (char)(seq->seq.s[j])=='t' || (char)(seq->seq.s[j])=='g' || (char)(seq->seq.s[j])=='c'){
					//printf("%c\n",seq->seq.s[j]);
					haplos->hap_seq[i][j-z]=seq->seq.s[j];
				}
				else{
					z++;
				}
			}
			*len_hap[i]=*len_hap[i]-z;
			i++;
		}
		gzclose(fp);				//close the file handler  
		kseq_destroy(seq);			//destroy seq  
	}
	
	//for(i=0;i<nr_hap;i++){
	//	for(j=0;j<*len_hap[i];j++){
	//		printf("%c",haplos->hap_seq[i][j]);
	//	}
	//	printf("\n");
	//}
	//for(i=0;i<nr_hap;i++){
	//	printf(">>>%s\n",haplos->hap_name[i]);
	//}
	

	
	/////////////////////////
	//Set up threads and random number generator
	/////////////////////////
	
	
	//pick a reference at random
	int mySeed=MYSEED;
	
	//set OMP_NUM_THREADS for parallel computations
	omp_set_num_threads(nrThreads);
	
	int myNrThreads=omp_get_max_threads();
	printf("\nNumber of threads used: %d\n\n",myNrThreads);
	
	gsl_rng* rng_seed;
	rng_seed=gsl_rng_alloc(gsl_rng_ranlxs2);
	gsl_rng_set(rng_seed,mySeed);
	
	gsl_rng** r = (gsl_rng**)malloc(myNrThreads*sizeof(gsl_rng*));
	for(i=0;i<myNrThreads;i++){
		r[i] = gsl_rng_alloc(gsl_rng_ranlxs2);
		int seed=gsl_rng_get(rng_seed);
		gsl_rng_set(r[i],seed);
	}
	
	

	
	/////////////////////////
	//Read in frequencies
	/////////////////////////
	
	double *frequencies;	
	frequencies= (double*) malloc(nr_hap*sizeof(double));
	
	for(i=0;i<nr_hap;i++){
		frequencies[i]=0;
	}
	
	if(HAPFREQ==1){
		int co=0;
		char eachLine[100];
		
		//read in frequencies from file
		FILE *ifp_freq = NULL;									
		ifp_freq = fopen(szFreqFile, "r");						
		
		if(ifp_freq){
			while(fgets(eachLine, 100, ifp_freq) != NULL){	
				//printf("%s",eachLine);	
				frequencies[co]=atof(eachLine);
				co=co+1;
			}
			fclose(ifp_freq);
		}
		else{
			fprintf(stderr, "Failed to open frequency file.\n");				
			exit(0);			
		}
		
		//check that frequencies sum up to 1
		double freqSum=0;
		for(i=0;i<nr_hap;i++){
			freqSum = freqSum + frequencies[i];
		}
		//printf("freqSum=%.6f\n",freqSum);
        //printf("freqSum-1=%.6f\n",fabs(freqSum-1));
        //printf("eps=%.20f\n",eps);

        
        if(fabs(freqSum-1)>eps){
            
            printf("Frequencies do not sum up to 1. Sum equals %.20f.\nRemainder will be added to the frequency of the last sequence.\n\n",freqSum);
            
            //exit(0);
            
        }
		else{
			//printf("Sum of frequencies is 1.\n");
		}
		//check if a frequency is supplied for every sequence
		if(co != nr_hap){
			printf("A frequency must be supplied for every sequence! Check your frequency file.\n\n");
			exit(0);
		}
	}
	
	
	////////////////
	//// Define 454 Noise
	////////////////
	//load noise distributions for 454
	double **dist0;
	double **dist1;
	double **dist2;
	double **dist3;
	double **dist4;
	double **dist5;
	double **dist6;
	double **dist7;
	double **dist8;
	double **dist9;	
	
	//allocate memory
	dist0 = (double **) malloc(1000*sizeof(double*));
	dist1 = (double **) malloc(1000*sizeof(double*));
	dist2 = (double **) malloc(1000*sizeof(double*));
	dist3 = (double **) malloc(1000*sizeof(double*));
	dist4 = (double **) malloc(1000*sizeof(double*));
	dist5 = (double **) malloc(1000*sizeof(double*));
	dist6 = (double **) malloc(1000*sizeof(double*));
	dist7 = (double **) malloc(1000*sizeof(double*));
	dist8 = (double **) malloc(1000*sizeof(double*));
	dist9 = (double **) malloc(1000*sizeof(double*));

	
	if(dist0 && dist1 && dist2 && dist3 && dist4 && dist5 && dist6 && dist7 && dist8 && dist9){
		for(i=0;i<1000;i++){
			dist0[i]= (double*) malloc(sizeof(double));
			dist0[i][0]=0;
			dist1[i]= (double*) malloc(sizeof(double));
			dist1[i][0]=0;
			dist2[i]= (double*) malloc(sizeof(double));
			dist2[i][0]=0;
			dist3[i]= (double*) malloc(sizeof(double));
			dist3[i][0]=0;
			dist4[i]= (double*) malloc(sizeof(double));
			dist4[i][0]=0;
			dist5[i]= (double*) malloc(sizeof(double));
			dist5[i][0]=0;
			dist6[i]= (double*) malloc(sizeof(double));
			dist6[i][0]=0;
			dist7[i]= (double*) malloc(sizeof(double));
			dist7[i][0]=0;
			dist8[i]= (double*) malloc(sizeof(double));
			dist8[i][0]=0;
			dist9[i]= (double*) malloc(sizeof(double));
			dist9[i][0]=0;
			
			if(!dist0[i] || !dist1[i] || !dist2[i] || !dist3[i] || !dist4[i] || !dist5[i] || !dist6[i] || !dist7[i] || !dist8[i] || !dist9[i]){				//check if memory was allocated
				printf("problem with memory allocation\n\n");
			}
		}
	}
	else{
		printf("problem with memory allocation\n\n");
	}
	
	double** dist[10];
	
	dist[0] = dist0;
	dist[1] = dist1;
	dist[2] = dist2;
	dist[3] = dist3;
	dist[4] = dist4;
	dist[5] = dist5;
	dist[6] = dist6;
	dist[7] = dist7;
	dist[8] = dist8;
	dist[9] = dist9;
	
	
	dist0=loadNoise(dist0,0);
	dist1=loadNoise(dist1,1);
	dist2=loadNoise(dist2,2);
	dist3=loadNoise(dist3,3);
	dist4=loadNoise(dist4,4);
	dist5=loadNoise(dist5,5);
	dist6=loadNoise(dist6,6);
	dist7=loadNoise(dist7,7);
	dist8=loadNoise(dist8,8);
	dist9=loadNoise(dist9,9);
	
	
	if(CYCLE_NR%4 != 0){
		printf("Number of flow cycles invalid.\n");
		CYCLE_NR=CYCLE_NR-(CYCLE_NR%4);
		printf("Will use %d flow cycles instead.\n\n",CYCLE_NR);
	}
	
	
	////////////////
	//// Define Illumina Noise
	////////////////
	
	double del_prob=0;
	double ins_prob=0;
	double sub_prob=-1;
	
	int del_count=0;
	int ins_count=0;
	int sub_count=0;
	int counting=0;
	int nr_del=0;
	int nr_ins=0;
	double rand_nuc=-1;
	int nucX=-1;
	
	
	////////////
	//Available Illumina noise profiles
	////////////

	int ILL_L = 0;

	FILE *ifp_illumina_noise_fwd = NULL;
	FILE *ifp_illumina_noise_rev = NULL;
	FILE *ifp_illumina_nucOcc_fwd = NULL;
	FILE *ifp_illumina_nucOcc_rev = NULL;
	FILE *ifp_illumina_del_fwd = NULL;
	FILE *ifp_illumina_del_rev = NULL;
	FILE *ifp_illumina_ins_fwd = NULL;
	FILE *ifp_illumina_ins_rev = NULL;

	
	char *szInsertDistFile = (char*)malloc(200*sizeof(char));
	int InsertDistNrElem=0;
	int InsertDistNrLen=0;
	
	///**********************************************************
	///**********************************************************
	///**********************************************************
	
	//Insert distribution

	if(INSERT_D==1){
		InsertDistNrElem=1932874; //the insert distribution is made up of xxx sequences (=inserts)
		InsertDistNrLen=301; //the insert distribution covers xx different insert lengths
		strcpy(szInsertDistFile,"Noise/insertDist_1.txt");
	}
	if(INSERT_D==2){
		InsertDistNrElem=888395;
		InsertDistNrLen=501;
		strcpy(szInsertDistFile,"Noise/insertDist_2.txt");
	}
	if(INSERT_D==3){
		InsertDistNrElem=1405048;
		InsertDistNrLen=750;
		strcpy(szInsertDistFile,"Noise/insertDist_3.txt");
	}
	//if(INSERT_D==4){
	//	InsertDistNrElem=1936608;
	//	InsertDistNrLen=726;
	//	strcpy(szInsertDistFile,"Noise/insertDist_ConCoct.txt");
	//}
	
	//printf("InsertDistNrElem=%d\n",InsertDistNrElem);
	//printf("InsertDistNrLen=%d\n",InsertDistNrLen);
	
	
    
	switch (ILL_PROFILE) {
        case 100:
        {
            //Do nothing if not simulating Illumina reads
            szReadsFormat="FASTA";
            break;
        }
        //
        //
        //Miseq Fusion Golay Amplicon 250bp (DS78)
		case 1:
		{
			ILL_L=250;
			szReadsFormat="FASTA";
			motif=0;
			
            if(MYAMPLICON!=1){
                printf("This error profile is designed for the simulation of amplicons! (Amplicons will be simulated. Choose another profile to simulate WGS.)");
            }
            
			MYAMPLICON=1;
			
			ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_Miseq_FG_Amp.txt","r");
			ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_Miseq_FG_Amp.txt","r");
			
			ifp_illumina_nucOcc_fwd = fopen("Noise/R1.ATGC_nucOcc_Miseq_FG_Amp.txt","r");
			ifp_illumina_nucOcc_rev = fopen("Noise/R2.ATGC_nucOcc_Miseq_FG_Amp.txt","r");
			
			ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_Miseq_FG_Amp.txt","r");
			ifp_illumina_del_rev = fopen("Noise/R2.errorDel_Miseq_FG_Amp.txt","r");
			ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_Miseq_FG_Amp.txt","r");
			ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_Miseq_FG_Amp.txt","r");

			if(Noise==2){				
			printf("Illumina V4 Amplicon Miseq 2x250bp (Fusion Golay, DS78) position & nucleotide specific noise profile.\n\n");
			}

			break;
		}

		//Miseq NexteraXT Amplicon 250bp (DS31)
		case 2:
		{
			ILL_L=250;
			szReadsFormat="FASTA";
			motif=0;
			
            if(MYAMPLICON!=1){
                printf("This error profile is designed for the simulation of amplicons! (Amplicons will be simulated. Choose another profile to simulate WGS.)");
            }
			MYAMPLICON=1;
			
			ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_Miseq_XT_Amp.txt","r");
			ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_Miseq_XT_Amp.txt","r");
			
			ifp_illumina_nucOcc_fwd = fopen("Noise/R1.ATGC_nucOcc_Miseq_XT_Amp.txt","r");
			ifp_illumina_nucOcc_rev = fopen("Noise/R2.ATGC_nucOcc_Miseq_XT_Amp.txt","r");
			
			ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_Miseq_XT_Amp.txt","r");
			ifp_illumina_del_rev = fopen("Noise/R2.errorDel_Miseq_XT_Amp.txt","r");
			ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_Miseq_XT_Amp.txt","r");
			ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_Miseq_XT_Amp.txt","r");

			if(Noise==2){			
			printf("Illumina V3/V4 Amplicon Miseq 2x250bp (NexteraXT, DS31) position & nucleotide specific noise profile.\n\n");
			}

			break;
		}

		//Miseq Dual Index Amplicon 250bp (DS60)
		case 3:
		{
			ILL_L=250;
			szReadsFormat="FASTA";
			motif=0;
			
            if(MYAMPLICON!=1){
                printf("This error profile is designed for the simulation of amplicons! (Amplicons will be simulated. Choose another profile to simulate WGS.)");
            }
			MYAMPLICON=1;
			
			ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_Miseq_DI_Amp.txt","r");
			ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_Miseq_DI_Amp.txt","r");
			
			ifp_illumina_nucOcc_fwd = fopen("Noise/R1.ATGC_nucOcc_Miseq_DI_Amp.txt","r");
			ifp_illumina_nucOcc_rev = fopen("Noise/R2.ATGC_nucOcc_Miseq_DI_Amp.txt","r");
			
			ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_Miseq_DI_Amp.txt","r");
			ifp_illumina_del_rev = fopen("Noise/R2.errorDel_Miseq_DI_Amp.txt","r");
			ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_Miseq_DI_Amp.txt","r");
			ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_Miseq_DI_Amp.txt","r");

			if(Noise==2){			
			printf("Illumina V4 Amplicon Miseq 2x250bp (Dual Index, DS60) position & nucleotide specific noise profile.\n\n");
			}

			break;
		}
			
		//Miseq Fusion Golay Amplicon 250bp (DS78)
		//Motif based
		case 4:
		{
			ILL_L=250;
			szReadsFormat="FASTA";
			motif=1;
			
            if(MYAMPLICON!=1){
                printf("This error profile is designed for the simulation of amplicons! (Amplicons will be simulated. Choose another profile to simulate WGS.)");
            }
			MYAMPLICON=1;
			
			ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_Miseq_FG_Amp.txt","r");
			ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_Miseq_FG_Amp.txt","r");

			ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_Miseq_FG_Amp.txt","r");
			ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_Miseq_FG_Amp.txt","r");

			ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_Miseq_FG_Amp.txt","r");
			ifp_illumina_del_rev = fopen("Noise/R2.motifDel_Miseq_FG_Amp.txt","r");
			ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_Miseq_FG_Amp.txt","r");
			ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_Miseq_FG_Amp.txt","r");

			if(Noise==2){			
			printf("Illumina V4 Amplicon Miseq 2x250bp (Fusion Golay, DS78) motif based noise profile.\n\n");
			}

			break;
		}
			
		//Miseq NexteraXT Amplicon 250bp (DS31)
		//Motif based
		case 5:
		{
			ILL_L=250;
			szReadsFormat="FASTA";
			motif=1;
			
            if(MYAMPLICON!=1){
                printf("This error profile is designed for the simulation of amplicons! (Amplicons will be simulated. Choose another profile to simulate WGS.)");
            }
			MYAMPLICON=1;
			
			ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_Miseq_XT_Amp.txt","r");
			ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_Miseq_XT_Amp.txt","r");

			ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_Miseq_XT_Amp.txt","r");
			ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_Miseq_XT_Amp.txt","r");

			ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_Miseq_XT_Amp.txt","r");
			ifp_illumina_del_rev = fopen("Noise/R2.motifDel_Miseq_XT_Amp.txt","r");
			ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_Miseq_XT_Amp.txt","r");
			ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_Miseq_XT_Amp.txt","r");

			if(Noise==2){			
			printf("Illumina V3/V4 Amplicon Miseq 2x250bp (NexteraXT, DS31) motif based noise profile.\n\n");
			}

			break;
		}
			
		//Miseq Dual Index Amplicon 250bp (DS60)
		//Motif based
		case 6:
		{
			ILL_L=250;
			szReadsFormat="FASTA";
			
            if(MYAMPLICON!=1){
                printf("This error profile is designed for the simulation of amplicons! (Amplicons will be simulated. Choose another profile to simulate WGS.)");
            }
			MYAMPLICON=1;
			motif=1;
			
			ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_Miseq_DI_Amp.txt","r");
			ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_Miseq_DI_Amp.txt","r");

			ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_Miseq_DI_Amp.txt","r");
			ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_Miseq_DI_Amp.txt","r");
			
			ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_Miseq_DI_Amp.txt","r");
			ifp_illumina_del_rev = fopen("Noise/R2.motifDel_Miseq_DI_Amp.txt","r");
			ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_Miseq_DI_Amp.txt","r");
			ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_Miseq_DI_Amp.txt","r");

			if(Noise==2){			
			printf("Illumina V4 Amplicon Miseq 2x250bp (Dual Index, DS60) motif based noise profile.\n\n");
			}

			break;
		}

         
        //HiSeq TruSeq 100bp (DS74)
        //position & nucleotide specific
        case 7:
        {
        ILL_L=100;
        szReadsFormat="FASTA";
        
        if(MYAMPLICON!=0){
             printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
        }
        MYAMPLICON=0;
        motif=0;
            
        ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_HiSeq_stdL_MG.txt","r");
        ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_HiSeq_stdL_MG.txt","r");
            
        ifp_illumina_nucOcc_fwd = fopen("Noise/R1_ATGC_nucO_HiSeq_StdL_MG.txt","r");
        ifp_illumina_nucOcc_rev = fopen("Noise/R2_ATGC_nucO_HiSeq_StdL_MG.txt","r");
            
        ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_HiSeq_StdL_MG.txt","r");
        ifp_illumina_del_rev = fopen("Noise/R2.errorDel_HiSeq_StdL_MG.txt","r");
        ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_HiSeq_StdL_MG.txt","r");
        ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_HiSeq_StdL_MG.txt","r");
            
        if(Noise==2){
            printf("Illumina metagenomic Hiseq noise profile 2x100bp for TruSeq library preparation method (position & nucleotide specific, MG DS74).\n\n");
        }
            
        break;
        }
    
  
        //HiSeq Nextera 100bp (DS70)
        //position & nucleotide specific
        case 8:
        {
            ILL_L=100;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=0;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_HiSeq_N_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_HiSeq_N_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1_ATGC_nucO_HiSeq_N_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2_ATGC_nucO_HiSeq_N_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_HiSeq_N_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.errorDel_HiSeq_N_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_HiSeq_N_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_HiSeq_N_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Hiseq noise profile 2x100bp for Nextera library preparation method (position & nucleotide specific, MG DS70).\n\n");
            }
            
            break;
        }

            
        //HiSeq NexteraXT 100bp (DS22)
        //position & nucleotide specific
        case 9:
        {
            ILL_L=100;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=0;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_HiSeq_NXT_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_HiSeq_NXT_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1_ATGC_nucO_HiSeq_NXT_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2_ATGC_nucO_HiSeq_NXT_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_HiSeq_NXT_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.errorDel_HiSeq_NXT_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_HiSeq_NXT_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_HiSeq_NXT_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Hiseq noise profile 2x100bp for NexteraXT library preparation method (position & nucleotide specific, MG DS22).\n\n");
            }
            
            break;
        }

            
            
        //HiSeq TruSeq 100bp (DS74)
        //motif-based
        case 10:
        {
            ILL_L=100;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=1;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_HiSeq_stdL_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_HiSeq_stdL_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_HiSeq_StdL_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_HiSeq_StdL_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_HiSeq_StdL_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.motifDel_HiSeq_StdL_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_HiSeq_StdL_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_HiSeq_StdL_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Hiseq noise profile 2x100bp for TruSeq library preparation method (motif-based, MG DS74).\n\n");
            }
            
            break;
        }
            
            
            
            
        //HiSeq Nextera 100bp (DS70)
        //motif-based
        case 11:
        {
            ILL_L=100;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=1;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_HiSeq_N_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_HiSeq_N_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_HiSeq_N_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_HiSeq_N_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_HiSeq_N_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.motifDel_HiSeq_N_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_HiSeq_N_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_HiSeq_N_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Hiseq noise profile 2x100bp for Nextera library preparation method (motif-based, MG DS70).\n\n");
            }
            
            break;
        }
            
            
        //HiSeq NexteraXT 100bp (DS22)
        //motif-based
        case 12:
        {
            ILL_L=100;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=1;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_HiSeq_NXT_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_HiSeq_NXT_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_HiSeq_NXT_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_HiSeq_NXT_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_HiSeq_NXT_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.motifDel_HiSeq_NXT_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_HiSeq_NXT_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_HiSeq_NXT_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Hiseq noise profile 2x100bp for NexteraXT library preparation method (motif-based, MG DS22).\n\n");
            }
            
            break;
        }
            
            
        //MiSeq Nextera 250bp (DS55)
        //position & nucleotide specific
        case 13:
        {
            ILL_L=250;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=0;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_MiSeq_N_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_MiSeq_N_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1_ATGC_nucO_MiSeq_N_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2_ATGC_nucO_MiSeq_N_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_MiSeq_N_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.errorDel_MiSeq_N_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_MiSeq_N_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_MiSeq_N_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Miseq noise profile 2x250bp for Nextera library preparation method (position & nucleotide specific, MG DS55).\n\n");
            }
            
            break;
        }
           
            
        //MiSeq NexteraXT 250bp (DS102)
        //position & nucleotide specific
        case 14:
        {
            ILL_L=250;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=0;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_MiSeq_NXT_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_MiSeq_NXT_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1_ATGC_nucO_MiSeq_NXT_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2_ATGC_nucO_MiSeq_NXT_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_MiSeq_NXT_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.errorDel_MiSeq_NXT_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_MiSeq_NXT_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_MiSeq_NXT_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Miseq noise profile 2x250bp for NexteraXT library preparation method (position & nucleotide specific, MG DS102).\n\n");
            }
            
            break;
        }

            
        //MiSeq Nextera 250bp (DS55)
        //motif-based
        case 15:
        {
            ILL_L=250;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=1;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_MiSeq_N_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_MiSeq_N_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_MiSeq_N_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_MiSeq_N_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_MiSeq_N_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.motifDel_MiSeq_N_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_MiSeq_N_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_MiSeq_N_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Miseq noise profile 2x250bp for Nextera library preparation method (motif-based, MG DS55).\n\n");
            }
            
            break;
        }

            
        //MiSeq NexteraXT 250bp (DS102)
        //motif-based
        case 16:
        {
            ILL_L=250;
            szReadsFormat="FASTA";
            
            if(MYAMPLICON!=0){
                printf("This error profile is designed for the simulation of WGS/metagenomics! (WGS will be simulated. Choose another profile to simulate amplicons.)");
            }
            MYAMPLICON=0;
            motif=1;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_MiSeq_NXT_MG.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_MiSeq_NXT_MG.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_MiSeq_NXT_MG.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_MiSeq_NXT_MG.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_MiSeq_NXT_MG.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.motifDel_MiSeq_NXT_MG.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_MiSeq_NXT_MG.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_MiSeq_NXT_MG.txt","r");
            
            if(Noise==2){
                printf("Illumina metagenomic Miseq noise profile 2x250bp for NexteraXT library preparation method (motif-based, MG DS102).\n\n");
            }
            
            break;
        }

            
            
		//case 7:
		// {//HiSeq for Concoct
		// ILL_L=100;
		// szReadsFormat="FASTA";
		//
        // if(MYAMPLICON!=0){
        //     printf("This error profile is designed for the simulation of WGS! (WGS will be simulated. Choose another profile to simulate amplicons.)");
        // }
		// MYAMPLICON=0;
		// motif=0;
			 
		// ifp_illumina_noise_fwd = fopen("Noise/R1_errorAll_ILLHiSeq_stdL_v0.3.txt","r");
		// ifp_illumina_noise_rev = fopen("Noise/R2_errorAll_ILLHiSeq_stdL_v0.3.txt","r");
		 
		// ifp_illumina_nucOcc_fwd = fopen("Noise/R1_ATGC_nucO_HiSeqStdL_v0.3.txt","r");
		// ifp_illumina_nucOcc_rev = fopen("Noise/R2_ATGC_nucO_HiSeqStdL_v0.3.txt","r");
		 
		// ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_HiSeqStdL_v0.3.txt","r");
		// ifp_illumina_del_rev = fopen("Noise/R2.errorDel_HiSeqStdL_v0.3.txt","r");
		// ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_HiSeqStdL_v0.3.txt","r");
		// ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_HiSeqStdL_v0.3.txt","r");

		//if(Noise==2){
		// printf("Illumina MG Hiseq noise profile for ConCoct.\n\n");
		// }

		// break;
		// }
						

			
        case 200:
        {//mockEP_nucleotideAndPositionSpecific_MG
            ILL_L=100;
            szReadsFormat="FASTA";
            
            MYAMPLICON=0;
            motif=0;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1_errorAll_mockEP2.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2_errorAll_mockEP2.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1_ATGC_nucO_mockEP.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2_ATGC_nucO_mockEP.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1_ATGC_del_mockEP.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2_ATGC_del_mockEP.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1_ATGC_ins_mockEP.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2_ATGC_ins_mockEP.txt","r");
            
            if(Noise==2){
                printf("Illumina mock noise profile (nucleotide and position specific).\n\n");
            }
            
            break;
        }
            
        //Motif based mock profile
        case 201:
        {
            ILL_L=100;
            szReadsFormat="FASTA";
            
            MYAMPLICON=0;
            motif=1;
            
            ifp_illumina_noise_fwd = fopen("Noise/R1.motifSub_mock.txt","r");
            ifp_illumina_noise_rev = fopen("Noise/R2.motifSub_mock.txt","r");
            
            ifp_illumina_nucOcc_fwd = fopen("Noise/R1.motifOcc_mock.txt","r");
            ifp_illumina_nucOcc_rev = fopen("Noise/R2.motifOcc_mock.txt","r");
            
            ifp_illumina_del_fwd = fopen("Noise/R1.motifDel_mock.txt","r");
            ifp_illumina_del_rev = fopen("Noise/R2.motifDel_mock.txt","r");
            ifp_illumina_ins_fwd = fopen("Noise/R1.motifIns_mock.txt","r");
            ifp_illumina_ins_rev = fopen("Noise/R2.motifIns_mock.txt","r");
            
            if(Noise==2){			
                printf("Illumina mock motif noise profile.\n\n");
            }
            
            break;
        }


            
		default:
		{
			ILL_L=250;
			szReadsFormat="FASTA";

			MYAMPLICON=1;
			
			ifp_illumina_noise_fwd = fopen("Noise/R1.errorAll_Miseq_FG_Amp.txt","r");
			ifp_illumina_noise_rev = fopen("Noise/R2.errorAll_Miseq_FG_Amp.txt","r");
			
			ifp_illumina_nucOcc_fwd = fopen("Noise/R1.ATGC_nucOcc_Miseq_FG_Amp.txt","r");
			ifp_illumina_nucOcc_rev = fopen("Noise/R2.ATGC_nucOcc_Miseq_FG_Amp.txt","r");
			
			ifp_illumina_del_fwd = fopen("Noise/R1.errorDel_Miseq_FG_Amp.txt","r");
			ifp_illumina_del_rev = fopen("Noise/R2.errorDel_Miseq_FG_Amp.txt","r");
			ifp_illumina_ins_fwd = fopen("Noise/R1.errorIns_Miseq_FG_Amp.txt","r");
			ifp_illumina_ins_rev = fopen("Noise/R2.errorIns_Miseq_FG_Amp.txt","r");

			if(Noise==2){			
			printf("No valid profile chosen.\nDefault Profile:\nIllumina Amplicon Miseq 2x250bp (Fusion Golay, DS78) noise profile.\n\n");
			}

			break;
		}
			
	}

    
    
    
	///**********************************************************
	///**********************************************************
	///**********************************************************
	
	
	
	if(Noise==2){
		READ_LENGTH=ILL_L;
	}
	
	////////////
	//if position and nucleotide specific profiles are used
	////////////
	//SUBSTITUTIONS
	//forward read
	double*** Ill_Noise_fwd = NULL;
	Ill_Noise_fwd = (double***) malloc(4*sizeof(double**));		//4 matrices with xxx rows and 4 columns
	//nucleotides: 0->"A",1->"T",2->"G",3->"C"
	for(j=0;j<4;j++){	
		Ill_Noise_fwd[j] = (double**) malloc(ILL_L*sizeof(double*));
		for(i=0;i<ILL_L;i++){
			Ill_Noise_fwd[j][i] = (double*) malloc(4*sizeof(double));
		}
	}

	//reverse read
	double*** Ill_Noise_rev = NULL;
	Ill_Noise_rev = (double***) malloc(4*sizeof(double**));		//4 matrices with xxx rows and 4 columns
	//nucleotides: 0->"A",1->"T",2->"G",3->"C"
	for(j=0;j<4;j++){	
		Ill_Noise_rev[j] = (double**) malloc(ILL_L*sizeof(double*));
		for(i=0;i<ILL_L;i++){
			Ill_Noise_rev[j][i] = (double*) malloc(4*sizeof(double));
		}
	}

	//Deletions on fwd and rev read (0->fwd, 1->rev)
	double*** Ill_Noise_Del = NULL;
	Ill_Noise_Del = (double***) malloc(2*sizeof(double**));		//2 matrices with xxx rows and 4 columns
	//nucleotides: 0->"A",1->"T",2->"G",3->"C"
	for(j=0;j<2;j++){	
		Ill_Noise_Del[j] = (double**) malloc(ILL_L*sizeof(double*));
		for(i=0;i<ILL_L;i++){
			Ill_Noise_Del[j][i] = (double*) malloc(4*sizeof(double));
		}
	}


	//Insertions on fwd and rev read (0->fwd, 1->rev)
	double*** Ill_Noise_Ins = NULL;
	Ill_Noise_Ins = (double***) malloc(2*sizeof(double**));		//2 matrices with xxx rows and 4 columns
	//nucleotides: 0->"A",1->"T",2->"G",3->"C"
	for(j=0;j<2;j++){	
		Ill_Noise_Ins[j] = (double**) malloc(ILL_L*sizeof(double*));
		for(i=0;i<ILL_L;i++){
			Ill_Noise_Ins[j][i] = (double*) malloc(4*sizeof(double));
		}
	}

	//initialise all of them
	for(k=0;k<4;k++){
		for(i=0;i<ILL_L;i++){
			for(j=0;j<4;j++){
				Ill_Noise_fwd[k][i][j] = -1;
				Ill_Noise_rev[k][i][j] = -1;
			}
		}
	}

	for(k=0;k<2;k++){
		for(i=0;i<ILL_L;i++){
			for(j=0;j<4;j++){
				Ill_Noise_Del[k][i][j] = -1;
				Ill_Noise_Ins[k][i][j] = -1;
			}
		}
	}


	//NUCLEOTIDE OCCURRENCES (order A -> T -> G -> C)
	int** nucATGC_fwd = NULL;
	nucATGC_fwd = (int**) malloc(4*sizeof(int*));

	int** nucATGC_rev = NULL;
	nucATGC_rev = (int**) malloc(4*sizeof(int*));

	for(j=0;j<4;j++){
		nucATGC_fwd[j] = (int*) malloc(ILL_L*sizeof(int));
		nucATGC_rev[j] = (int*) malloc(ILL_L*sizeof(int));
	}

	for(k=0;k<4;k++){
		for(i=0;i<ILL_L;i++){
			nucATGC_fwd[k][i]=-1;
			nucATGC_rev[k][i]=-1;
		}
	}

	
	////////////
	//if motif based profiles are used
	////////////
	//SUBSTITUTIONS
	//forward read
	double** Ill_motif_fwd = NULL;
	Ill_motif_fwd = (double**) malloc(64*sizeof(double*));		//64 rows (one for each motif) and 4 columns (which nucleotide got substituted)
	//nucleotides: 0->"A",1->"C",2->"G",3->"T"
	for(j=0;j<64;j++){	
		Ill_motif_fwd[j] = (double*) malloc(4*sizeof(double));
	}
	
	//reverse read
	double** Ill_motif_rev = NULL;
	Ill_motif_rev = (double**) malloc(64*sizeof(double*));		//64 rows and 4 columns 
	//nucleotides: 0->"A",1->"C",2->"G",3->"T"
	for(j=0;j<64;j++){	
		Ill_motif_rev[j] = (double*) malloc(4*sizeof(double));
	}
	
	//Deletions
	double** Ill_motif_Del_fwd = NULL;
	Ill_motif_Del_fwd = (double**) malloc(64*sizeof(double*));		//64 rows and 4 columns (which nucleotide got deleted)
	//nucleotides: 0->"A",1->"C",2->"G",3->"T"
	for(j=0;j<64;j++){	
		Ill_motif_Del_fwd[j] = (double*) malloc(4*sizeof(double));
	}

	double** Ill_motif_Del_rev = NULL;
	Ill_motif_Del_rev = (double**) malloc(64*sizeof(double*));		//64 rows and 4 columns (which nucleotide got deleted)
	//nucleotides: 0->"A",1->"C",2->"G",3->"T"
	for(j=0;j<64;j++){	
		Ill_motif_Del_rev[j] = (double*) malloc(4*sizeof(double));
	}

	
	//Insertions 
	double** Ill_motif_Ins_fwd = NULL;
	Ill_motif_Ins_fwd = (double**) malloc(64*sizeof(double*));		//64 rows and 4 columns (which nucleotide got inserted)
	//nucleotides: 0->"A",1->"C",2->"G",3->"T"
	for(j=0;j<64;j++){	
		Ill_motif_Ins_fwd[j] = (double*) malloc(4*sizeof(double));
	}

	double** Ill_motif_Ins_rev = NULL;
	Ill_motif_Ins_rev = (double**) malloc(64*sizeof(double*));		//64 rows and 4 columns (which nucleotide got inserted)
	//nucleotides: 0->"A",1->"C",2->"G",3->"T"
	for(j=0;j<64;j++){	
		Ill_motif_Ins_rev[j] = (double*) malloc(4*sizeof(double));
	}

	
	//initialise all of them
	for(i=0;i<64;i++){
		for(j=0;j<4;j++){
			Ill_motif_fwd[i][j] = -1;
			Ill_motif_rev[i][j] = -1;
			Ill_motif_Del_fwd[i][j] = -1;
			Ill_motif_Del_rev[i][j] = -1;
			Ill_motif_Ins_fwd[i][j] = -1;
			Ill_motif_Ins_rev[i][j] = -1;
	
		}
	}

	
	//NUCLEOTIDE OCCURRENCES (order A -> C -> G -> T)
	int** nucATGC_motif_fwd = NULL;
	nucATGC_motif_fwd = (int**) malloc(64*sizeof(int*));
	
	for(j=0;j<64;j++){
		nucATGC_motif_fwd[j] = (int*) malloc(4*sizeof(int));
	}
	
	int** nucATGC_motif_rev = NULL;
	nucATGC_motif_rev = (int**) malloc(64*sizeof(int*));
	
	for(j=0;j<64;j++){
		nucATGC_motif_rev[j] = (int*) malloc(4*sizeof(int));
	}
	
	
	for(i=0;i<64;i++){
		for(j=0;j<4;j++){
			nucATGC_motif_fwd[i][j]=-1;
			nucATGC_motif_rev[i][j]=-1;
		}
	}
	

	/////////////
	///Read-in Illumina nucleotide occurrence file
	/////////////	
	if(Noise==2){
	

		//if position and nucleotide specific profiles are used
		if(motif==0){

			loadIllNucOcc(ifp_illumina_nucOcc_fwd,ILL_L,nucATGC_fwd);
			loadIllNucOcc(ifp_illumina_nucOcc_rev,ILL_L,nucATGC_rev);
			
			
			/////////////
			///Read-in Illumina error profile
			/////////////	
			
			loadIllNoise(ifp_illumina_noise_fwd,ILL_L,Ill_Noise_fwd,nucATGC_fwd);
			loadIllNoise(ifp_illumina_noise_rev,ILL_L,Ill_Noise_rev,nucATGC_rev);
			
			
			/////////////
			///Read-in Illumina deletions
			/////////////	

			//printf("\n\nRead-in Illumina deletion profile ...\n\n");
			
			loadIllIndel(ifp_illumina_del_fwd, ILL_L,Ill_Noise_Del[0],nucATGC_fwd);
			loadIllIndel(ifp_illumina_del_rev, ILL_L,Ill_Noise_Del[1],nucATGC_rev);
			
			
			/////////////
			///Read-in Illumina insertions
			/////////////	
			
			//printf("\n\nRead-in Illumina insertion profile ...\n\n");
			
			loadIllIndel(ifp_illumina_ins_fwd, ILL_L,Ill_Noise_Ins[0],nucATGC_fwd);
			loadIllIndel(ifp_illumina_ins_rev, ILL_L,Ill_Noise_Ins[1],nucATGC_rev);
		}
		
		//if motif based profiles are used
		if(motif==1){

			//occurrence of motifs
			loadMotifOcc(ifp_illumina_nucOcc_fwd,nucATGC_motif_fwd);
			loadMotifOcc(ifp_illumina_nucOcc_rev,nucATGC_motif_rev);

			//Substitutions
			loadMotif(ifp_illumina_noise_fwd,Ill_motif_fwd,nucATGC_motif_fwd);
			loadMotif(ifp_illumina_noise_rev,Ill_motif_rev,nucATGC_motif_rev);
			
			//Deletions
			loadMotif(ifp_illumina_del_fwd,Ill_motif_Del_fwd,nucATGC_motif_fwd);
			loadMotif(ifp_illumina_del_rev,Ill_motif_Del_rev,nucATGC_motif_rev);
			
			//Insertions
			loadMotifIns(ifp_illumina_ins_fwd,Ill_motif_Ins_fwd,nucATGC_motif_fwd);
			loadMotifIns(ifp_illumina_ins_rev,Ill_motif_Ins_rev,nucATGC_motif_rev);

			
            /*
			for(i=0;i<64;i++){
				printf("%f\t%f\t%f\t%f\n",Ill_motif_fwd[i][0],Ill_motif_fwd[i][1],Ill_motif_fwd[i][2],Ill_motif_fwd[i][3]);
			}
			printf("\n\n");
			for(i=0;i<64;i++){
				printf("%f\t%f\t%f\t%f\n",Ill_motif_rev[i][0],Ill_motif_rev[i][1],Ill_motif_rev[i][2],Ill_motif_rev[i][3]);
			}
			printf("\n\n");
			
			
			for(i=0;i<64;i++){
				printf("%f\t%f\t%f\t%f\n",Ill_motif_Ins_fwd[i][0],Ill_motif_Ins_fwd[i][1],Ill_motif_Ins_fwd[i][2],Ill_motif_Ins_fwd[i][3]);
			}
			printf("\n\n");
			for(i=0;i<64;i++){
				printf("%f\t%f\t%f\t%f\n",Ill_motif_Ins_rev[i][0],Ill_motif_Ins_rev[i][1],Ill_motif_Ins_rev[i][2],Ill_motif_Ins_rev[i][3]);
			}
			printf("\n\n");
			
			
			for(i=0;i<64;i++){
				printf("%f\t%f\t%f\t%f\n",Ill_motif_Del_fwd[i][0],Ill_motif_Del_fwd[i][1],Ill_motif_Del_fwd[i][2],Ill_motif_Del_fwd[i][3]);
			}
			printf("\n\n");
			for(i=0;i<64;i++){
				printf("%f\t%f\t%f\t%f\n",Ill_motif_Del_rev[i][0],Ill_motif_Del_rev[i][1],Ill_motif_Del_rev[i][2],Ill_motif_Del_rev[i][3]);
			}
			printf("\n\n");
             */
        
		
		}
		
		
		
	}

    
    
    
    
    
    //////////////////////////
    //Check if amplicon size is sufficient
    
    for(i=0;i<nr_hap;i++){
        //printf("%d\n",*len_hap[i]);
        if(*len_hap[i]<READ_LENGTH){
            printf("!!!!!!!!!!!!!Reference sequence is shorter than desired read length (plus an extra 20bp to accommodate deletions)!!!!!!!!!!\n\n");
            printf("%s\n\n",haplos->hap_name[i]);
            exit(0);
        }
            
    }

    
    
    
    
    
    
	
	////////////////
	//// Read-in insert distribution (insert length comprises whole fragment (incl. reads))
	////////////////
	inD* InDist = (inD*) malloc(InsertDistNrLen*sizeof(inD));
	if(InDist){
		for(i=0;i<InsertDistNrLen;i++){
			InDist[i].freq=(int*)malloc(sizeof(int));
			InDist[i].len=(int*)malloc(sizeof(int));
		}
	}
	else{
		printf("problem with memory allocation\n\n");
	}
	
	InDist=loadInsertDist(InDist,szInsertDistFile);
	
	
	
	/////////////
	/// Define PCR noise
	/////////////
	
	double nucA[4] = {0};
	double nucC[4] = {0};
	double nucT[4] = {0};
	double nucG[4] = {0};
	
	if(PCR_NOISE==1){
		
		printf("Changing probabilities for the nucleotides (A,C,T,G) due to PCR noise:\n\n");
		
		//Define changing probabilities of A, C, T or G respectively
		nucA[1] = exp(-11.619259);		//probability that A changes to C
		nucA[2] = exp(-11.694004);		//probability that A changes to T
		nucA[3] = exp(-7.748623);		//probability that A changes to G
		nucA[0] = 1 - nucA[1] - nucA[2] - nucA[3];		//probability that A stays the same
		
		nucC[0] = exp(-11.619259);		//probability that C changes to A
		nucC[2] = exp(-7.619657);		//probability that C changes to T
		nucC[3] = exp(-12.852562);		//probability that C changes to G
		nucC[1] = 1 - nucC[0] - nucC[2] - nucC[3];		//probability that C changes to C
		
		nucT[0] = exp(-11.694004);		//probability that T changes to A
		nucT[1] = exp(-7.619657);		//probability that T changes to C
		nucT[3] = exp(-10.964048);		//probability that T changes to G
		nucT[2] = 1 - nucT[0] - nucT[1] - nucT[3];	//probability that T changes to T
		
		nucG[0] = exp(-7.748623);		//probability that G changes to A
		nucG[1] = exp(-12.852562);		//probability that G changes to C
		nucG[2] = exp(-10.964048);		//probability that G changes to T
		nucG[3] = 1 - nucG[0] - nucG[1] - nucG[2];		//probability that G changes to G
		
		printf("%f %f %f %f\n", nucA[0],nucA[1],nucA[2],nucA[3]);
		printf("%f %f %f %f\n", nucC[0],nucC[1],nucC[2],nucC[3]);
		printf("%f %f %f %f\n", nucT[0],nucT[1],nucT[2],nucT[3]);
		printf("%f %f %f %f\n", nucG[0],nucG[1],nucG[2],nucG[3]);
		printf("\n--------------------------------------\n\n");
		
	}
	else{
		//no PCR noise -> nucleotides stay the same
		nucA[1] = 0;		//probability that A changes to C
		nucA[2] = 0;		//probability that A changes to T
		nucA[3] = 0;		//probability that A changes to G
		nucA[0] = 1;		//probability that A stays the same

		nucC[0] = 0;		//probability that C changes to A
		nucC[2] = 0;		//probability that C changes to T
		nucC[3] = 0;		//probability that C changes to G
		nucC[1] = 1;		//probability that C changes to C
		
		nucT[0] = 0;		//probability that T changes to A
		nucT[1] = 0;		//probability that T changes to C
		nucT[3] = 0;		//probability that T changes to G
		nucT[2] = 1;		//probability that T changes to T
		
		nucG[0] = 0;		//probability that G changes to A
		nucG[1] = 0;		//probability that G changes to C
		nucG[2] = 0;		//probability that G changes to T
		nucG[3] = 1;		//probability that G changes to G
		
	}
	
	
    
    if(Noise==1){
        MYPAIREDEND=0;
    }
	
	//////////////
	///Output Files
	//////////////
	printf("Prepare output files...\n");
	
	FILE *ifp_reads_F = NULL;									
	FILE *ifp_reads_R = NULL;	
	FILE *ifp_reads_F_dat = NULL;
	char *szReadsFileF;
	char *szReadsFileR;
	char *szReadsFileF_dat;
	char *szReadsFileR_dat;
	int homopolymer=1;
	int nextPos=1;
	
	char c[100];
	sprintf(c, "%d", mySeed);

	char readFile_fwd[100];
	char readFile_rev[100];
	strcpy(readFile_fwd,SampleName);
	strcpy(readFile_rev,SampleName);
	strcat(readFile_fwd,"_R1_seed");
	strcat(readFile_rev,"_R2_seed");
	strcat(readFile_fwd,c);
	strcat(readFile_rev,c);
	sprintf(c,"_CPUs%d",nrThreads);
	strcat(readFile_fwd,c);
	strcat(readFile_rev,c);
	szReadsFileF=readFile_fwd;
	szReadsFileR=readFile_rev;
	
	
	szReadsFileF_dat=readFile_fwd;
	
	//printf("szReadsFileF=%s\n",szReadsFileF);
	
    
    
	if(!strncmp(szReadsFormat,FASTQ,5)){
		if(MYPAIREDEND==1){
			strcat(szReadsFileF,".fastq");
			strcat(szReadsFileR,".fastq");
		}
		else{
			strcat(szReadsFileF,".fastq");
		}
	}
	else{
		if(MYPAIREDEND==1){
			strcat(szReadsFileF,".fasta");
			strcat(szReadsFileR,".fasta");
		}
		else{
			strcat(szReadsFileF,".fasta");
		}
	}

    

    if(MYPAIREDEND==1){

        if(access(szReadsFileF,F_OK)==-1){
            ifp_reads_F = fopen(szReadsFileF, "w");
        }
        else{
            printf("\n\n Output file already exists!\n\n");
            exit(0);
        }
        
        if(access(szReadsFileR,F_OK)==-1){
            ifp_reads_R = fopen(szReadsFileR, "w");
        }
        else{
            printf("\n\n Output file already exists!\n\n");
            exit(0);
        }
        
	}
	else{
        if(access(szReadsFileF,F_OK)==-1){
            ifp_reads_F = fopen(szReadsFileF, "w");
        }
        else{
            printf("\n\n Output file already exists!\n\n");
            exit(0);
        }
        
        
	}
	
	if(Noise==1){
		strcat(szReadsFileF_dat,".dat");
            ifp_reads_F_dat = fopen(szReadsFileF_dat, "w");
	}
    

	//////////////////
	//Initionalise Strand information for reference sequences
	//////////////////
	
	printf("Initialise strands ...\n");
	
	int** Strand=(int**)malloc(nr_hap*sizeof(int*));
	
	for(i=0;i<nr_hap;i++){
		Strand[i]=(int*)malloc(sizeof(int));					// 0->+Strand	1->-Strand
		if(MYAMPLICON==0){
			//printf("%d\n",(int)gsl_rng_uniform_int(r[0],2));
			*Strand[i] = (int)gsl_rng_uniform_int(r[0],2);		//draw random number to determine if the reference is the + strand or the - strand
		}
		else{
			*Strand[i]=0;			//for amplicons we are assuming that all sequences in the data base have the same orientation, the reads will all originate from strand 0
		}
	}
	
	//for(i=0;i<nr_hap;i++){
    //    printf("%i\n",*Strand[i]);
    //}
	
	
	//No noise: sample from the distribution Gaussian(READ_LENGTH,STANDARD_DEVIATION)
	int read_len_gauss = 0;			//will contain the read length

	
	int n=0, n2=0;
	
	int sub_count_total[myNrThreads];
	int ins_count_total[myNrThreads];
	int del_count_total[myNrThreads];
	int SeqError_total[myNrThreads];
	int pcrError_total[myNrThreads];
	for(i=0;i<myNrThreads;i++){
		sub_count_total[i]=0;
		ins_count_total[i]=0;
		del_count_total[i]=0;
		SeqError_total[i]=0;
		pcrError_total[i]=0;
	}
	
	
	////////////////////////////
	////////////////////////////
	////////////////////////////
	////////////////////////////

	
	if(ifp_reads_F){
		if(MYPAIREDEND==1){
			if(ifp_reads_R){
				Total_Nr_Reads=(int)Total_Nr_Reads/2;
				printf("\nGenerating %d forward and %d reverse reads.\n\n",Total_Nr_Reads,Total_Nr_Reads);
			}
			else{
				printf("Problem with reverse read file.\n\n");
			}
		}
		
	
		//////////////
		///Main loop
		//////////////
	
		#pragma omp parallel for default(none) shared(r,myNrThreads,HAPFREQ,frequencies,nr_hap,STANDARD_DEVIATION,READ_LENGTH,InDist,szReadsFormat,ifp_reads_F,ifp_reads_R,ifp_reads_F_dat,MYPAIREDEND,haplos,len_hap,std_char,MYAMPLICON,Noise,pcrError,SeqError,dist0,dist1,dist2,dist3,dist4,dist5,dist6,dist7,dist8,dist9,dist,nucA,nucC,nucG,nucT,ILL_L,Ill_Noise_fwd,Ill_Noise_rev,FASTQ,pcrError_total,SeqError_total,del_count_total,ins_count_total,sub_count_total,Total_Nr_Reads,Strand,CYCLE_NR,FLOW_ORDER,FLOW_ORDER_2,PCR_NOISE,LENGTH,Ill_Noise_Del,Ill_Noise_Ins,ins_sum,ins_sum_rev,InsertDistNrElem,InsertDistNrLen,Ill_motif_fwd,Ill_motif_rev,Ill_motif_Del_fwd,Ill_motif_Del_rev,Ill_motif_Ins_fwd,Ill_motif_Ins_rev,nucATGC_motif_fwd,nucATGC_motif_rev,motif) private(k,l,read_len_gauss,pickHap,randX,checkReadLen,checkReadLen_R,insertSize,c_ins,sum_ins,insertSize_freq,position,i,checkDig,homopolymer,nextPos,randNoise,noiseCount,nr_del,nr_ins,ins_prob,del_prob,rand_nuc,nucX,Nuc100,Nuc100_2,sum100,sub_count,j,del_count,ins_count,sub_prob,n2)		
		for(n=0;n<myNrThreads;n++){
			
			int NrMotif=-1;
			
			n2=0;
			int nthreads = omp_get_thread_num();
			
			while(n2<(((int)(Total_Nr_Reads/myNrThreads))+1)){
		
				k=(n2*myNrThreads)+nthreads;
		
				if(k<Total_Nr_Reads){
				
 					//printf("k=%d\n",k);
					//printf("Thread %d starting ... k=%d \n",nthreads,k);
				
                    
					double nuc_change_prob = 0;
					int myCurrentThread=omp_get_thread_num();
				
					if(Noise==0){
						LENGTH=2*READ_LENGTH;
					}
					if(Noise==1){
						LENGTH=3*CYCLE_NR;
					}
					if(Noise==2){
						LENGTH=2*ILL_L;
					}
					double** eachRead_dat = (double**)malloc((CYCLE_NR/4)*sizeof(double*));
					for(l=0;l<(CYCLE_NR/4);l++){
						eachRead_dat[l]=(double*)malloc(4*sizeof(double));					
					}
					for(l=0;l<(CYCLE_NR/4);l++){
						for(i=0;i<4;i++){
							eachRead_dat[l][i]=0;
						}
					}
					
					//make array that contains the read name which is written to the file at the end of the loop
					//make array that contains the read which is written to the file at the end of the loop; initialise with \n for read name
					char* eachRead_Name = (char*) malloc(MAX_NAME_LEN * sizeof(char));
                    
                    
					char* eachRead = (char*) malloc(LENGTH * sizeof(char) + sizeof("\n"));
					sprintf(eachRead,"\n");
					
					//same for reverse read
					char* eachRead_Name_rev = (char*) malloc(MAX_NAME_LEN*sizeof(char));
					char* eachRead_rev = (char*) malloc(LENGTH * sizeof(char)+ sizeof("\n"));
					sprintf(eachRead_rev,"\n");

					char help[0];
					
                    
                    //at the beginning of a new read the length is set to zero
					checkReadLen=0;
					checkReadLen_R=0;
					pcrError=0;
					SeqError=0;
					ins_count=0;
					del_count=0;
					sub_count=0;
					
					if(k%10000==0){
						printf("Generated %d reads ...\n",k);
					}

					//find the next sequence from which the read origninates
					if(HAPFREQ==1){		//abundance levels must be provided in file
						randX = gsl_rng_uniform(r[myCurrentThread]);
						pickHap = findHap(randX, frequencies, nr_hap);
						//printf("Thread %d starting ... randX=%f \n",nthreads, randX);
					}
					else{
						//randomly choose a haplotype
						pickHap = gsl_rng_uniform_int(r[myCurrentThread],nr_hap);
						//printf("Thread %d starting ... pickHap=%d \n",nthreads, pickHap);
					}
					//printf("pickHap=%d\n",pickHap);
					
                    
                    
                    //randomly choose read length
                    read_len_gauss = gsl_ran_gaussian(r[myCurrentThread],STANDARD_DEVIATION) + READ_LENGTH;			//read length
                    //printf("Thread %d starting ... read_len_gauss=%d \n",nthreads, read_len_gauss);

                    
                    
					int iter=0;
					
					//compute insertsize
					if(MYPAIREDEND==1){
						if(MYAMPLICON==0){			//WGS
							insertSize=0;
							c_ins=0;
							sum_ins=0;

                            
                            insertSize_freq=(int)gsl_rng_uniform_int(r[myCurrentThread],InsertDistNrElem);
							//printf("Thread %d starting ... insertSize_freq=%d \n",nthreads, insertSize_freq);
							//printf("insertSize_freq=%d\n",insertSize_freq);
                            
                            
							for(c_ins=0;c_ins<InsertDistNrLen;c_ins++){
								if(insertSize_freq < (*(InDist[c_ins].freq) + sum_ins)){
									//printf("insertSize_freq=%d\n",insertSize_freq);
									insertSize=*(InDist[c_ins].len);
                                    
									c_ins=LargeNr;
								}
								else{
									sum_ins = sum_ins + *(InDist[c_ins].freq);
								}
							}
							if(insertSize==0){
								printf("Problem with insertSize ...\n");
								exit(0);
							}
                            
                            //for noise free reads the insert needs be large enough to accommodate the read length
                            if(Noise==0){
                                while(insertSize<read_len_gauss){
                                    if(iter>MAX_ITER){
                                        printf("\n\nFragment size distribution seems to be inappropriate for desired read length.\n\n");
                                        exit(0);
                                    }
                                    insertSize_freq=(int)gsl_rng_uniform_int(r[myCurrentThread],InsertDistNrElem);
                                    
                                    for(c_ins=0;c_ins<InsertDistNrLen;c_ins++){
                                        if(insertSize_freq < (*(InDist[c_ins].freq) + sum_ins)){
                                            //printf("insertSize_freq=%d\n",insertSize_freq);
                                            insertSize=*(InDist[c_ins].len);
                                            
                                            c_ins=LargeNr;
                                        }
                                        else{
                                            sum_ins = sum_ins + *(InDist[c_ins].freq);
                                        }
                                    }
                                    if(insertSize==0){
                                        printf("Problem with insertSize ...\n");
                                        exit(0);
                                    }
                                    
                                    iter++;
                                }
                            }

                            
						}
						else{
							 if(MYAMPLICON==1){
								 insertSize=(*len_hap[pickHap]);
							 }
						}
					}
					else{
						insertSize=0;
					}
					//printf("insertSize=%d\n",insertSize);		 
					
					
					//Starting position of the read		
					if(MYAMPLICON==0){
						position = gsl_rng_uniform_int(r[myCurrentThread],*len_hap[pickHap]);			//randomly choose a starting point for the read (uniformly distributed)	
						//printf("Thread %d starting ... position=%d \n",nthreads, position);
					}
					else{
						if(MYAMPLICON==1){
							position = 0;		//for amplicons we want all reads to start at the same position (=the beginning of the sequence) 
						}
						else{
							printf("*************** Problem with MYAMPLICON!\n\n");
						}
					}
					
                    
                    if(Noise==0){
					}
					else{
                            read_len_gauss=ILL_L;
                        
					}
					
                    //printf("read_len_gauss=%d\n\n",read_len_gauss);
                    
                    
					iter=0;
					
					if(MYAMPLICON==0){
						//make sure the starting position leaves enough space for the read (max of 200 iterations)
                     	if(*Strand[pickHap]==0){
							if(MYPAIREDEND==1){
								while(position>(int)(*len_hap[pickHap]-(UPPER_LIM_INS+insertSize))){
									if(iter>MAX_ITER){
										printf("Read length exceeds length of one of the reference sequences? Can't find starting position.\n\n");
										exit(0);
									}
									position = gsl_rng_uniform_int(r[myCurrentThread],*len_hap[pickHap]);
									//printf("Thread %d starting ... position=%d \n",nthreads, position);
									iter++;
								}
							}
							else{
								while(position>(int)(*len_hap[pickHap]-(read_len_gauss+UPPER_LIM_INS))){
									if(iter>MAX_ITER){
										printf("Read length exceeds length of one of the reference sequences? Can't find starting position.\n\n");
										exit(0);
									}
									position = gsl_rng_uniform_int(r[myCurrentThread],*len_hap[pickHap]);
									//printf("Thread %d starting ... position=%d \n",nthreads, position);
									iter++;
								}
							}
						}
						else{
							if(*Strand[pickHap]==1){
								if(MYPAIREDEND==1){
									while((*len_hap[pickHap]-position)<(int)(UPPER_LIM_INS+insertSize)){
										if(iter>MAX_ITER){
											printf("Read length exceeds length of one of the reference sequences? Can't find starting position.\n\n");
											exit(0);
										}
										position = gsl_rng_uniform_int(r[myCurrentThread],*len_hap[pickHap]);
										//printf("Thread %d starting ... position=%d \n",nthreads, position);
										iter++;
									}
								}
								else{
									while((*len_hap[pickHap]-position)<(int)(read_len_gauss+UPPER_LIM_INS+1)){
										if(iter>MAX_ITER){
											printf("Read length exceeds length of one of the reference sequences? Can't find starting position.\n\n");
											exit(0);
										}
										position = gsl_rng_uniform_int(r[myCurrentThread],*len_hap[pickHap]);
										//printf("Thread %d starting ... position=%d \n",nthreads, position);
										iter++;
									}
								}
							}
							else{
								printf("Problem with Strand (4) ...\n");
								exit(0);
							}
						}
					}
					
					/////////////
					//prints read name to file
					/////////////
					if(!strncmp(szReadsFormat,FASTQ,5)){
						char *p = haplos->hap_name[pickHap];		//print name without first character (">")
						//p++;
						//fprintf(ifp_reads_F, "@%s_nr%d_R1\n", p,k);
						sprintf(eachRead_Name, "@%s_nr%d_R1", p,k);
						
						if(MYPAIREDEND){
							//fprintf(ifp_reads_R, "@%s_nr%d_R2\n", p,k);
							sprintf(eachRead_Name_rev, "@%s_nr%d_R2", p,k);					
						}
					}
					else{
						//printf("Reads in fasta format.\n");
						if(*Strand[pickHap]==0){
							//fprintf(ifp_reads_F, ">%s_nr%d_+_R1\n", haplos->hap_name[pickHap],k);
							sprintf(eachRead_Name, ">%s_nr%d_+_R1", haplos->hap_name[pickHap],k);
						}
						else{
							if(*Strand[pickHap]==1){
								//fprintf(ifp_reads_F, ">%s_nr%d_-_R1\n", haplos->hap_name[pickHap],k);
								sprintf(eachRead_Name, ">%s_nr%d_-_R1", haplos->hap_name[pickHap],k);
							}
							else{
								printf("Problem with Strand ...\n");
								exit(0);
							}
						}
						if(MYPAIREDEND){
							if(*Strand[pickHap]==0){
								//fprintf(ifp_reads_R, "%s_nr%d_-_R2\n",haplos->hap_name[pickHap],k);
								sprintf(eachRead_Name_rev, ">%s_nr%d_-_R2",haplos->hap_name[pickHap],k);
							}
							else{
								if(*Strand[pickHap]==1){
									//fprintf(ifp_reads_R, "%s_nr%d_+_R2\n",haplos->hap_name[pickHap],k);
									sprintf(eachRead_Name_rev, ">%s_nr%d_+_R2",haplos->hap_name[pickHap],k);
								}
								else{
									printf("Problem with Strand ...\n");
									exit(0);
								}
							}
						}
					}
					
                    //For 454
					//set up sequence as max. possible read
					char** MaxSeq = (char**)malloc(CYCLE_NR*sizeof(char*));
					for(l=0;l<CYCLE_NR;l++){
						MaxSeq[l]=(char*)malloc(sizeof(char));
						*MaxSeq[l]='X';
					}
					
					//For Illumina
					//set up sequence as max. possible read
					char** MaxSeq_2 = (char**)malloc((ILL_L+UPPER_LIM_DEL)*sizeof(char*));
					for(l=0;l<(ILL_L+UPPER_LIM_DEL);l++){
						MaxSeq_2[l]=(char*)malloc(sizeof(char));
						*MaxSeq_2[l]='X';
					}
					
					char** MaxSeq_2_rev = (char**)malloc((ILL_L+UPPER_LIM_DEL)*sizeof(char*));
					for(l=0;l<(ILL_L+UPPER_LIM_DEL);l++){
						MaxSeq_2_rev[l]=(char*)malloc(sizeof(char));
						*MaxSeq_2_rev[l]='X';
					}
					
					
					
					//////////////////
					//Introduce noise to the read
					//////////////////
					
					switch(Noise){
						//no noise
						case 0: 
						{	
							//printf("no noise ...\n");
							for(i=position;i<(int)(position+read_len_gauss);i++){
								
								if(*Strand[pickHap]==0){	
									help[0] = haplos->hap_seq[pickHap][i];
								}
								else{
									if(*Strand[pickHap]==1){
                                        help[0] = complementNuc(haplos->hap_seq[pickHap][*len_hap[pickHap]-i]);
										
									}
									else{
										printf("Problem with strand (noise 0) ...\n");
										exit(0);
									}
								}

								strncat(eachRead,help,1);
								//reverse read
								if(MYPAIREDEND==1){
									
									
									if((*Strand[pickHap]+1)%2==0){	
										help[0] = haplos->hap_seq[pickHap][*len_hap[pickHap]-2*position-insertSize+i];
									}
									else{
										if((*Strand[pickHap]+1)%2==1){
											help[0] = complementNuc(haplos->hap_seq[pickHap][insertSize+2*position-i]);
											
										}
										else{
											printf("Problem with strand (noise 0) ...\n");
											exit(0);
										}
									}
									strncat(eachRead_rev,help,1);
								}
								checkReadLen++;
							}
							break;
							exit(0);
						}
							
						//add 454 noise
						case 1:
						{
							//printf("Add 454 noise...\n");
					
							int diff=0;
							
                            //Extract max. possible sequence
							//Add PCR noise if desired					
							for(i=position;i<position+CYCLE_NR;i++){
                                
								if(i<(*len_hap[pickHap]+1) && i>-1){
									nuc_change_prob=gsl_rng_uniform(r[myCurrentThread]);
									//printf("Thread %d starting ... nuc_change_prob=%f \n",nthreads, nuc_change_prob);
									if(*Strand[pickHap]==0){
										*MaxSeq[i-position]=addPCRNoise(haplos->hap_seq[pickHap][i],nuc_change_prob,nucA,nucC,nucG,nucT);			//add PCR noise?
										if(haplos->hap_seq[pickHap][i] != *MaxSeq[i-position]){
											pcrError++;
										}
									}
									else{
										if(*Strand[pickHap]==1){
											*MaxSeq[i-position]=complementNuc(addPCRNoise(haplos->hap_seq[pickHap][*len_hap[pickHap]-i],nuc_change_prob,nucA,nucC,nucG,nucT));
											if(haplos->hap_seq[pickHap][*len_hap[pickHap]-i] != complementNuc(*MaxSeq[i-position])){
												pcrError++;
											}
										}
										else{
											printf("Problem with strand (noise 0) ...\n");
											exit(0);
										}
									}
									
								}
							}
							
                                                        i=0;
							for(l=0;l<CYCLE_NR/4;l++){
								
                                
								///////First we flow FLOW_ORDER[0] (e.g. T)
								//if it is a T
								if(*MaxSeq[i] == FLOW_ORDER[0] || *MaxSeq[i] == FLOW_ORDER_2[0]) {
									homopolymer=1;
									nextPos=1;

                                    while (*MaxSeq[i+nextPos] == FLOW_ORDER[0] || *MaxSeq[i+nextPos] == FLOW_ORDER_2[0])  {			//count how often the same nucleotide appears
										homopolymer++;
										nextPos++;
									}
									
 
                                    
									//only have data for homopolymers up to length 9
									if(homopolymer>9){
										diff=homopolymer-9;
										homopolymer=9;
									}
									
                                    
									randNoise=gsl_rng_uniform(r[myCurrentThread]);
									noiseCount=addNoise(randNoise, dist[homopolymer]);
									
									//print flow value to .dat file
									eachRead_dat[l][0]=noiseCount+diff;
									
									//print nucleotides to .fasta file
									for(j=0;j<(round(noiseCount)+diff);j++){
										//strncat(eachRead,FLOW_ORDER[0],1);
										help[0] = FLOW_ORDER[0];
										strncat(eachRead,help,1);
										checkReadLen++;
									}
                                    
									if(homopolymer != round(noiseCount)){
										SeqError=SeqError+abs(homopolymer-round(noiseCount));
									}
									i=i+nextPos;
								}
								//if it is not a T
								else{
									//printf("No T ...\n");
									randNoise=gsl_rng_uniform(r[myCurrentThread]);
									//printf("Thread %d starting ... randNoise=%f \n",nthreads,randNoise);
									noiseCount=addNoise(randNoise, dist[0]);
									
									//print flow value to .dat file
									eachRead_dat[l][0]=noiseCount;
									
									//print nucleotides to .fasta file
									for(j=0;j<round(noiseCount);j++){
										//strncat(eachRead,FLOW_ORDER[0],1);
										help[0] = FLOW_ORDER[0];
										strncat(eachRead,help,1);
										checkReadLen++;
										//printf("%c",'T');
									}
									if(0 != round(noiseCount)){
										SeqError=SeqError+abs(homopolymer-round(noiseCount));
									}
								}
								
								//if it is an A
								if(*MaxSeq[i] == FLOW_ORDER[1] || *MaxSeq[i] == FLOW_ORDER_2[1]) {
                                    homopolymer=1;
									nextPos=1;
									
									while (*MaxSeq[i+nextPos] == FLOW_ORDER[1] || *MaxSeq[i+nextPos] == FLOW_ORDER_2[1]) {			//count how often the same nucleotide appears
										homopolymer++;
										nextPos++;
									}
									
 
                                    //only have data for homopolymers up to length 9
									if(homopolymer>9){
										diff=homopolymer-9;
										homopolymer=9;
									}
									
									randNoise=gsl_rng_uniform(r[myCurrentThread]);
									//printf("Thread %d starting ... randNoise=%f \n",nthreads,randNoise);
									noiseCount=addNoise(randNoise, dist[homopolymer]);
									//print flow value to .dat file
									eachRead_dat[l][1]=noiseCount+diff;
									
									//print nucleotides to .fasta file
									for(j=0;j<(round(noiseCount)+diff);j++){
										//strncat(eachRead,FLOW_ORDER[1],1);
										help[0] = FLOW_ORDER[1];
										strncat(eachRead,help,1);
										checkReadLen++;
										//printf("%c",FLOW_ORDER[1]);
									}
									if(homopolymer != round(noiseCount)){
										SeqError=SeqError+abs(homopolymer-round(noiseCount));
									}
									i=i+nextPos;
								}
								//if it is not an A
								else{
									//printf("No A ...\n");
									randNoise=gsl_rng_uniform(r[myCurrentThread]);
									//printf("Thread %d starting ... randNoise=%f \n",nthreads,randNoise);
									noiseCount=addNoise(randNoise, dist[0]);

									//print flow value to .dat file
									eachRead_dat[l][1]=noiseCount;								
									
									//print nucleotides to .fasta file
									for(j=0;j<round(noiseCount);j++){
										//strncat(eachRead,FLOW_ORDER[1],1);
										help[0] = FLOW_ORDER[1];
										strncat(eachRead,help,1);
										checkReadLen++;
										//printf("%c",'T');
									}
									if(0 != round(noiseCount)){
										SeqError=SeqError+abs(homopolymer-round(noiseCount));
									}
									
								}
								
								//if it is a C
								if(*MaxSeq[i] == FLOW_ORDER[2] || *MaxSeq[i] == FLOW_ORDER_2[2]) {
                                    

                                    homopolymer=1;
									nextPos=1;
									
									while (*MaxSeq[i+nextPos] == FLOW_ORDER[2] || *MaxSeq[i+nextPos] == FLOW_ORDER_2[2]) {			//count how often the same nucleotide appears
										homopolymer++;
										nextPos++;
									}
									
 
                                    //only have data for homopolymers up to length 9
									if(homopolymer>9){
										diff=homopolymer-9;
										homopolymer=9;
									}
									
									randNoise=gsl_rng_uniform(r[myCurrentThread]);
									noiseCount=addNoise(randNoise, dist[homopolymer]);

									//print flow value to .dat file
									eachRead_dat[l][2]=noiseCount+diff;
									
									//print nucleotides to .fasta file
									for(j=0;j<(round(noiseCount)+diff);j++){
										//strncat(eachRead,FLOW_ORDER[2],1);
										help[0] = FLOW_ORDER[2];
										strncat(eachRead,help,1);
										checkReadLen++;
										//printf("%c",FLOW_ORDER[2]);
									}
									if(homopolymer != round(noiseCount)){
										SeqError=SeqError+abs(homopolymer-round(noiseCount));
									}
									
									i=i+nextPos;
								}
								//if it is not an C
								else{
									//printf("No C ...\n");
									randNoise=gsl_rng_uniform(r[myCurrentThread]);
									noiseCount=addNoise(randNoise, dist[0]);
									
									//print flow value to .dat file
									eachRead_dat[l][2]=noiseCount;
									
									//print nucleotides to .fasta file
									for(j=0;j<round(noiseCount);j++){
										//strncat(eachRead,FLOW_ORDER[2],1);
										help[0] = FLOW_ORDER[2];
										strncat(eachRead,help,1);
										checkReadLen++;
										//printf("%c",FLOW_ORDER[2]);
									}
									if(0 != round(noiseCount)){
										SeqError=SeqError+abs(homopolymer-round(noiseCount));
									}
								}
								
								
								//if it is a G
								if(*MaxSeq[i] == FLOW_ORDER[3] || *MaxSeq[i] == FLOW_ORDER_2[3]) {
                                    
 
                                    homopolymer=1;
									nextPos=1;
									
									while (*MaxSeq[i+nextPos] == FLOW_ORDER[3] || *MaxSeq[i+nextPos] == FLOW_ORDER_2[3]) {			//count how often the same nucleotide appears
										homopolymer++;
										nextPos++;
									}
                                    
 
                                    //only have data for homopolymers up to length 9
									if(homopolymer>9){
										diff=homopolymer-9;
										homopolymer=9;
									}
									
                                    
									randNoise=gsl_rng_uniform(r[myCurrentThread]);
                                    
                                    
									noiseCount=addNoise(randNoise, dist[homopolymer]);
                                    
                                    
									//print flow value to .dat file
									eachRead_dat[l][3]=noiseCount+diff;
									
									//print nucleotides to .fasta file
									for(j=0;j<(round(noiseCount)+diff);j++){
										//strncat(eachRead,FLOW_ORDER[3],1);
										help[0] = FLOW_ORDER[3];
										strncat(eachRead,help,1);
										checkReadLen++;
										//printf("%c",FLOW_ORDER[3]);
									}
                                    
                                    
                                    
									if(homopolymer != round(noiseCount)){
										SeqError=SeqError+abs(homopolymer-round(noiseCount));
									}
									i=i+nextPos;
                                    
                                    
								}
								//if it is not an G
								else{
									//printf("No G ...\n");
									randNoise=gsl_rng_uniform(r[myCurrentThread]);
									noiseCount=addNoise(randNoise, dist[0]);
									
									//print flow value to .dat file
									eachRead_dat[l][3]=noiseCount;
									
									//print nucleotides to .fasta file
									for(j=0;j<round(noiseCount);j++){
										//strncat(eachRead,FLOW_ORDER[3],1);
										help[0] = FLOW_ORDER[3];
										strncat(eachRead,help,1);
										checkReadLen++;
										//printf("%c",FLOW_ORDER[3]);
									}
									if(0 != round(noiseCount)){
										SeqError=SeqError+abs(homopolymer-round(noiseCount));
									}
								}
                                
                                
							}
                            
                            

                            
							break;
						}
							
						//add Illumina noise
						case 2:
						{
							//printf("Add Illumina noise \n");
							//Extract max. possible sequence
							
				
							l=0;
							for(i=position;i<position+ILL_L+UPPER_LIM_DEL;i++){

								//If no pcr noise -> just copies string
								if(i<*len_hap[pickHap] && i>-1){
									nuc_change_prob=gsl_rng_uniform(r[myCurrentThread]);
									//printf("Thread %d starting ... nuc_change_prob=%f \n",nthreads,nuc_change_prob);
									if(*Strand[pickHap]==0){
										*MaxSeq_2[i-position]=addPCRNoise(haplos->hap_seq[pickHap][i],nuc_change_prob,nucA,nucC,nucG,nucT);			//add PCR noise?
										if(haplos->hap_seq[pickHap][i] != *MaxSeq_2[i-position]){
											pcrError++;
										}
									}
									else{
										if(*Strand[pickHap]==1){
											*MaxSeq_2[i-position]=complementNuc(addPCRNoise(haplos->hap_seq[pickHap][*len_hap[pickHap]-i],nuc_change_prob,nucA,nucC,nucG,nucT));
											if(haplos->hap_seq[pickHap][*len_hap[pickHap]-i] != complementNuc(*MaxSeq_2[i-position])){
												pcrError++;
											}
										}
										else{
											printf("Problem with strand (noise 0) ...\n");
											exit(0);
										}
									}
								}
							
								if(MYPAIREDEND==1){
									nuc_change_prob=gsl_rng_uniform(r[myCurrentThread]);
									//printf("Thread %d starting ... nuc_change_prob=%f \n",nthreads,nuc_change_prob);
									if((*Strand[pickHap]+1)%2==0){
										*MaxSeq_2_rev[l]=addPCRNoise(haplos->hap_seq[pickHap][*len_hap[pickHap]-2*position-insertSize+i],nuc_change_prob,nucA,nucC,nucG,nucT);			//add PCR noise?
										if(haplos->hap_seq[pickHap][*len_hap[pickHap]-2*position-insertSize+i] != *MaxSeq_2_rev[l]){
											pcrError++;
										}
										l++;
									}
									else{
										if((*Strand[pickHap]+1)%2==1){
											*MaxSeq_2_rev[l]=complementNuc(addPCRNoise(haplos->hap_seq[pickHap][insertSize+2*position-i],nuc_change_prob,nucA,nucC,nucG,nucT));
											if(haplos->hap_seq[pickHap][insertSize+2*position-i] != complementNuc(*MaxSeq_2_rev[l])){
												pcrError++;
											}
											l++;
										}
										else{
											printf("Problem with strand (noise 0) ...\n");
											exit(0);
										}
									}
									
								}
							}
							
							
							
							//R1 read (has specific profile) 
							nr_del=0;
							nr_ins=0;
							checkReadLen=0;
							
							for(i=0;i<(ILL_L+nr_del-nr_ins);i++){
								
								Nuc100=nucTOint(*MaxSeq_2[i]);
								Nuc100_2=nucTOint2(*MaxSeq_2[i]);
								
								//dram random number to determine if a del occurs
								del_prob = gsl_rng_uniform(r[myCurrentThread]);
								
								////////////////
								//position and nucleotide specific profiles
								////////////////
								if(motif==0){
									//Did a deletion occur?
									//if(DEL_Illumina>=del_prob){
									if((Ill_Noise_Del[0][i-nr_del+nr_ins][0]+Ill_Noise_Del[0][i-nr_del+nr_ins][1]+Ill_Noise_Del[0][i-nr_del+nr_ins][2]+Ill_Noise_Del[0][i-nr_del+nr_ins][3])>del_prob){
                                        nr_del++;
										del_count++;
									}
									else{  
										//Did an insertion occur?
										ins_prob = gsl_rng_uniform(r[myCurrentThread]);
										//each row sum represents the probability that an insertion occurs at that position
										ins_sum=Ill_Noise_Ins[0][i-nr_del+nr_ins][0] + Ill_Noise_Ins[0][i-nr_del+nr_ins][1] + Ill_Noise_Ins[0][i-nr_del+nr_ins][2] + Ill_Noise_Ins[0][i-nr_del+nr_ins][3];

										if(ins_sum>=ins_prob){
											//printf("Got an insertion ...\n\n");
											//determine nucleotide that got inserted
											rand_nuc=gsl_rng_uniform(r[myCurrentThread]);
											
											if(rand_nuc<=(Ill_Noise_Ins[0][i-nr_del+nr_ins][0]/ins_sum)){
												strncat(eachRead,"A",1);
												checkReadLen++;
												
											}
											else{
												if(rand_nuc<=((Ill_Noise_Ins[0][i-nr_del+nr_ins][0] + Ill_Noise_Ins[0][i-nr_del+nr_ins][1])/ins_sum)){
													strncat(eachRead,"T",1);
													checkReadLen++;
													
												}
												else{
													if(rand_nuc<=((Ill_Noise_Ins[0][i-nr_del+nr_ins][0] + Ill_Noise_Ins[0][i-nr_del+nr_ins][1] + Ill_Noise_Ins[0][i-nr_del+nr_ins][2])/ins_sum)){
														strncat(eachRead,"G",1);
														checkReadLen++;
														
													}
													else{
														strncat(eachRead,"C",1);
														checkReadLen++;
														
													}
												}
											}
											nr_ins++;
											ins_count++;
										}
										
										//consider substitution for the original nucleotide
										sub_prob = gsl_rng_uniform(r[myCurrentThread]);	
										if(i<ILL_L+nr_del-nr_ins){			//if last position of the read is not reached yet
										
                                            sum100=Ill_Noise_fwd[Nuc100][i-nr_del+nr_ins][0];
												if(sub_prob<=sum100){
                                                strncat(eachRead,"A",1);
												checkReadLen++;
												sub_count++;
											}
											else{
												sum100=sum100 + Ill_Noise_fwd[Nuc100][i-nr_del+nr_ins][1];
												if(sub_prob<=sum100){
													
													strncat(eachRead,"T",1);
													checkReadLen++;
													sub_count++;
												}
												else{
													sum100=sum100 + Ill_Noise_fwd[Nuc100][i-nr_del+nr_ins][2];
													if(sub_prob<=sum100){
                                                        strncat(eachRead,"G",1);
														checkReadLen++;
														sub_count++;
													}
													else{
														sum100=sum100 + Ill_Noise_fwd[Nuc100][i-nr_del+nr_ins][3];
														if(sub_prob<=sum100){
															strncat(eachRead,"C",1);
															checkReadLen++;
															sub_count++;
														}
														else{
															//no substitution occurs
															help[0] = *MaxSeq_2[i];
															strncat(eachRead,help,1);
															checkReadLen++;
														}
													}
												}
											}
										}
										//else{
										//	printf("Problem with read length during the substitutions process for Illumina.\n\n");
										//}
									}

								}
								
								////////////////
								//motif based profiles
								////////////////
								if(motif==1){
									
									//need to consider 3 preceding bases -> nothing to do for i<3
									if(i<3){
										help[0] = *MaxSeq_2[i];
										strncat(eachRead,help,1);
										checkReadLen++;
										continue;
									}
									
									
									//Get motif preceding current position
									NrMotif=(nucTOint2(*MaxSeq_2[i-3])*16) + (nucTOint2(*MaxSeq_2[i-2])*4) + nucTOint2(*MaxSeq_2[i-1]);
									
									//Did a deletion occur?
									if(Ill_motif_Del_fwd[NrMotif][Nuc100_2]>del_prob){
										nr_del++;
										del_count++;
									}
									else{  
										
										//Did an insertion occur?
										ins_prob = gsl_rng_uniform(r[myCurrentThread]);
										
										ins_sum=Ill_motif_Ins_fwd[NrMotif][0]+Ill_motif_Ins_fwd[NrMotif][1]+Ill_motif_Ins_fwd[NrMotif][2]+Ill_motif_Ins_fwd[NrMotif][3];
                                        
                                        if(ins_sum>=ins_prob){
											//printf("Got an insertion ...\n\n");
											rand_nuc=gsl_rng_uniform(r[myCurrentThread]);
											
											if(rand_nuc<=(Ill_motif_Ins_fwd[NrMotif][0]/ins_sum)){
												strncat(eachRead,"A",1);
												checkReadLen++;
											}
											else{
												if(rand_nuc<=((Ill_motif_Ins_fwd[NrMotif][0] + Ill_motif_Ins_fwd[NrMotif][1])/ins_sum)){
													strncat(eachRead,"C",1);
													checkReadLen++;
												}
												else{
													if(rand_nuc<=((Ill_motif_Ins_fwd[NrMotif][0] + Ill_motif_Ins_fwd[NrMotif][1] + Ill_motif_Ins_fwd[NrMotif][2])/ins_sum)){
														strncat(eachRead,"G",1);
														checkReadLen++;
													}
													else{
														strncat(eachRead,"T",1);
														checkReadLen++;
													}
												}
											}
											nr_ins++;
											ins_count++;
										}
										
										
										//consider substitution for the original nucleotide
										sub_prob = gsl_rng_uniform(r[myCurrentThread]);	
										if(i<ILL_L+nr_del-nr_ins){			//if last position of the read is not reached yet
											
											if(sub_prob<Ill_motif_fwd[NrMotif][Nuc100_2]){
												rand_nuc=gsl_rng_uniform(r[myCurrentThread]);
												if(rand_nuc < 0.25){
													strncat(eachRead,"A",1);
												}
												else{
													if(rand_nuc < 0.5){
														strncat(eachRead,"C",1);
													}
													else{
														if(rand_nuc < 0.75){
															strncat(eachRead,"G",1);
														}
														else{
															strncat(eachRead,"T",1);
														}
													}
												}
												checkReadLen++;
												sub_count++;
											}
											else{
												//no substitution occurs
												help[0] = *MaxSeq_2[i];
												strncat(eachRead,help,1);
												checkReadLen++;
											}
											
											
											
										}
										
									}
								}									
							
							
							
							
							}
							
								
							if(MYPAIREDEND==1){
								//printf("Start reverse reads\n");
								nr_del=0;
								nr_ins=0;
								checkReadLen_R=0;
								
								for(i=0;i<(ILL_L+nr_del-nr_ins);i++){
									
									Nuc100=nucTOint(*MaxSeq_2_rev[i]);
									Nuc100_2=nucTOint2(*MaxSeq_2_rev[i]);
									
									//dram random number to determine if a del occurs
									del_prob = gsl_rng_uniform(r[myCurrentThread]);
									
									
									
									////////////////
									//position and nucleotide specific profiles
									////////////////
									if(motif==0){
										//Did a deletion occur?
										if((Ill_Noise_Del[1][i-nr_del+nr_ins][0]+Ill_Noise_Del[1][i-nr_del+nr_ins][1]+Ill_Noise_Del[1][i-nr_del+nr_ins][2]+Ill_Noise_Del[1][i-nr_del+nr_ins][3])>del_prob){
											nr_del++;
											del_count++;
											
										}
										else{  
											//Did an insertion occur?
											ins_prob = gsl_rng_uniform(r[myCurrentThread]);
									
                                            //each row sum represents the probability that an insertion occurs at that position
											ins_sum_rev=Ill_Noise_Ins[1][i-nr_del+nr_ins][0] + Ill_Noise_Ins[1][i-nr_del+nr_ins][1] + Ill_Noise_Ins[1][i-nr_del+nr_ins][2] + Ill_Noise_Ins[1][i-nr_del+nr_ins][3];
											if(ins_sum_rev>=ins_prob){
												rand_nuc=gsl_rng_uniform(r[myCurrentThread]);
												
												if(rand_nuc<=(Ill_Noise_Ins[1][i-nr_del+nr_ins][0]/ins_sum_rev)){
													strncat(eachRead_rev,"A",1);
													checkReadLen_R++;
												}
												else{
													if(rand_nuc<=((Ill_Noise_Ins[1][i-nr_del+nr_ins][0] + Ill_Noise_Ins[1][i-nr_del+nr_ins][1])/ins_sum_rev)){
														strncat(eachRead_rev,"T",1);
														checkReadLen_R++;
													}
													else{
														if(rand_nuc<=((Ill_Noise_Ins[1][i-nr_del+nr_ins][0] + Ill_Noise_Ins[1][i-nr_del+nr_ins][1] + Ill_Noise_Ins[1][i-nr_del+nr_ins][2])/ins_sum_rev)){
															strncat(eachRead_rev,"G",1);
															checkReadLen_R++;
														}
														else{
															strncat(eachRead_rev,"C",1);
															checkReadLen_R++;
														}
													}
												}
												nr_ins++;
												ins_count++;
											}
											
											//consider substitution for the original nucleotide
											sub_prob = gsl_rng_uniform(r[myCurrentThread]);	
											if(i<ILL_L+nr_del-nr_ins){			//if last position of the read is not reached yet
												
												sum100=Ill_Noise_rev[Nuc100][i-nr_del+nr_ins][0];
												if(sub_prob<=sum100){
													strncat(eachRead_rev,"A",1);
													checkReadLen_R++;
													sub_count++;
												}
												else{
													sum100=sum100 + Ill_Noise_rev[Nuc100][i-nr_del+nr_ins][1];
													if(sub_prob<=sum100){
														strncat(eachRead_rev,"T",1);
														checkReadLen_R++;
														sub_count++;
													}
													else{
														sum100=sum100 + Ill_Noise_rev[Nuc100][i-nr_del+nr_ins][2];
														if(sub_prob<=sum100){
															strncat(eachRead_rev,"G",1);
															checkReadLen_R++;
															sub_count++;
														}
														else{
															sum100=sum100 + Ill_Noise_rev[Nuc100][i-nr_del+nr_ins][3];
															if(sub_prob<=sum100){
																strncat(eachRead_rev,"C",1);
																checkReadLen_R++;
																sub_count++;
															}
															else{
																//no substitution occurs
																help[0] = *MaxSeq_2_rev[i];
																strncat(eachRead_rev,help,1);
																checkReadLen_R++;
															}
														}
													}
												}
											}
											//else{
											//	printf("Problem with read length during the substitutions process for Illumina.\n\n");
											//}
										}
									}
									
									////////////////
									//motif based profiles
									////////////////
									if(motif==1){
										
										//need to consider 3 preceding bases -> nothing to do for i<3
										if(i<3){
											help[0] = *MaxSeq_2_rev[i];
											strncat(eachRead_rev,help,1);
											checkReadLen_R++;
											continue;
										}
										
										
										//Get motif preceding current position
										NrMotif=(nucTOint2(*MaxSeq_2_rev[i-3])*16) + (nucTOint2(*MaxSeq_2_rev[i-2])*4) + nucTOint2(*MaxSeq_2_rev[i-1]);
										
										//Did a deletion occur?
										if(Ill_motif_Del_rev[NrMotif][Nuc100_2]>del_prob){
											nr_del++;
											del_count++;
										}
										else{  
											
											//Did an insertion occur?
											ins_prob = gsl_rng_uniform(r[myCurrentThread]);
											
											ins_sum=Ill_motif_Ins_rev[NrMotif][0]+Ill_motif_Ins_rev[NrMotif][1]+Ill_motif_Ins_rev[NrMotif][2]+Ill_motif_Ins_rev[NrMotif][3];
											
											if(ins_sum>=ins_prob){
												rand_nuc=gsl_rng_uniform(r[myCurrentThread]);
												
												if(rand_nuc<=(Ill_motif_Ins_rev[NrMotif][0]/ins_sum)){
													strncat(eachRead_rev,"A",1);
													checkReadLen_R++;
												}
												else{
													if(rand_nuc<=((Ill_motif_Ins_rev[NrMotif][0] + Ill_motif_Ins_rev[NrMotif][1])/ins_sum)){
														strncat(eachRead_rev,"C",1);
														checkReadLen_R++;
													}
													else{
														if(rand_nuc<=((Ill_motif_Ins_rev[NrMotif][0] + Ill_motif_Ins_rev[NrMotif][1] + Ill_motif_Ins_rev[NrMotif][2])/ins_sum)){
															strncat(eachRead_rev,"G",1);
															checkReadLen_R++;
														}
														else{
															strncat(eachRead_rev,"T",1);
															checkReadLen_R++;
														}
													}
												}
												nr_ins++;
												ins_count++;
											}
											
											
											//consider substitution for the original nucleotide
											sub_prob = gsl_rng_uniform(r[myCurrentThread]);	
											if(i<ILL_L+nr_del-nr_ins){			//if last position of the read is not reached yet
												
												if(sub_prob<Ill_motif_rev[NrMotif][Nuc100_2]){
													rand_nuc=gsl_rng_uniform(r[myCurrentThread]);
													if(rand_nuc < 0.25){
														strncat(eachRead_rev,"A",1);
													}
													else{
														if(rand_nuc < 0.5){
															strncat(eachRead_rev,"C",1);
														}
														else{
															if(rand_nuc < 0.75){
																strncat(eachRead_rev,"G",1);
															}
															else{
																strncat(eachRead_rev,"T",1);
															}
														}
													}
													checkReadLen_R++;
													sub_count++;
												}
												else{
													//no substitution occurs
													help[0] = *MaxSeq_2_rev[i];
													strncat(eachRead_rev,help,1);
													checkReadLen_R++;													
												}
												
												
											}
											
										}
									}									
									
									
									
									
								}
							}
							break;
						}
							
					}
					
                    
					fprintf(ifp_reads_F,"%s%s\n",eachRead_Name,eachRead);
					
					if(MYPAIREDEND==1){
						fprintf(ifp_reads_R,"%s%s\n",eachRead_Name_rev,eachRead_rev);
					}
					
					
                    
					char* dat_temp = (char*)malloc(2*CYCLE_NR*sizeof(double)); 
					char dat_temp2[100] = {0};
					
					sprintf(dat_temp2,"%f ",eachRead_dat[0][0]);
					strcpy(dat_temp,dat_temp2);
					
					sprintf(dat_temp2,"%f ",eachRead_dat[0][1]);
					strcat(dat_temp,dat_temp2);
					
					sprintf(dat_temp2,"%f ",eachRead_dat[0][2]);
					strcat(dat_temp,dat_temp2);
					
					sprintf(dat_temp2,"%f ",eachRead_dat[0][3]);
					strcat(dat_temp,dat_temp2);
					
					for(l=1;l<(CYCLE_NR/4);l++){
						for(j=0;j<4;j++){
							//printf("j=%d\n",j);
							sprintf(dat_temp2,"%f ",eachRead_dat[l][j]);
							strcat(dat_temp,dat_temp2);
						}
					}
					
                    
                    
					//also print to .dat file
					if(Noise==1){
						fprintf(ifp_reads_F_dat,"%s\n%s\n",eachRead_Name,dat_temp);
						//printf("%s\n%s\n",eachRead_Name,dat_temp);
					}
					
                    
                    
					switch(Noise){
						case 2:	
							if(checkReadLen!=READ_LENGTH){
								printf("Read number %d\n",k);
								printf("Read length=%d\n",READ_LENGTH);
								printf("checkReadLen=%d\n",checkReadLen);
								printf("Read length is not correct ...\n");
								exit(0);
							}
							if(MYPAIREDEND==1){	
								if(checkReadLen_R!=READ_LENGTH){
									printf("Read number %d\n",k);
									printf("Read length=%d\n",READ_LENGTH);
									printf("checkReadLen_R=%d\n",checkReadLen_R);
									printf("Read length is not correct (rev)...\n");
									exit(0);
								}
							}
					}
                    
                    
					//printf("del_count=%d\n",del_count);
					//printf("ins_count=%d\n",ins_count);
					//printf("del_count_total=%d\n",del_count_total);
					//printf("ins_count_total=%d\n",ins_count_total);
					
					pcrError_total[nthreads]=pcrError_total[nthreads]+pcrError;
					SeqError_total[nthreads]=SeqError_total[nthreads]+SeqError;			
					ins_count_total[nthreads]=ins_count_total[nthreads]+ins_count;
					del_count_total[nthreads]=del_count_total[nthreads]+del_count;
					sub_count_total[nthreads]=sub_count_total[nthreads]+sub_count;
							
                    
					for(j=0;j<CYCLE_NR;j++){
						free(MaxSeq[j]);
					}
					free(MaxSeq);
					
                    
                    
					for(j=0;j<(ILL_L+UPPER_LIM_DEL);j++){
						free(MaxSeq_2[j]);
						free(MaxSeq_2_rev[j]);
					}
					free(MaxSeq_2);
					free(MaxSeq_2_rev);
					
					for(l=0;l<(CYCLE_NR/4);l++){
						free(eachRead_dat[l]);
					}
					free(eachRead_dat);
					

                    free(eachRead);
                    

                    free(eachRead_Name);
 
                    free(eachRead_rev);
					free(eachRead_Name_rev);

                    free(dat_temp);
                    

                    
				}
			
				n2++;
			}
			
		}
	
		
		
		
		////////////////////
		//end of k loop (and parallel computations)
		////////////////////
		
		fclose(ifp_reads_F);
		
		if(MYPAIREDEND==1){
			fclose(ifp_reads_R);
		}
		if(Noise==1){
			fclose(ifp_reads_F_dat);
		}
		if(MYPAIREDEND==1){
			printf("\n-----------> Reads are saved in file %s and %s\n\n",szReadsFileF,szReadsFileR);
		}
		else{
			printf("\n-----------> Reads are saved in file %s\n\n",szReadsFileF);
		}
		
		if(!strncmp(szReadsFormat,FASTQ,5)){
			printf("\nStandard fastq file with ASCII(phred score + 33)\n\n");
		}
	}
	else{
		// code for file open failure
		fprintf(stderr, "Failed to open file %s\n",szReadsFileF);		// stderr
		exit(0);			
	}
	
	
	
	int tempSub=0;
	int tempIns=0;
	int tempDel=0;
	int tempPCR=0;
	int tempSeq=0;
	for(i=0;i<myNrThreads;i++){
		tempSub=tempSub+sub_count_total[i];
		tempIns=tempIns+ins_count_total[i];
		tempDel=tempDel+del_count_total[i];
		tempPCR=tempPCR+pcrError_total[i];
		tempSeq=tempSeq+SeqError_total[i];
	}
	
	if(PCR_NOISE==1){
		printf("Number of PCR errors: %d\n\n",tempPCR);
	}
	if(Noise==1){
		printf("Number of 454 errors: %d\n\n",tempSeq);
	}
	
	if(Noise==2){
		printf("Number of Illumina errors:\n Insertions: %d \t Deletions: %d \n",tempIns,tempDel);
		printf("Number of Illumina errors: Substitutions: %d \n\n",tempSub);
	}
    
    
    if(nrThreads > 1){
        printf("!!!!Don't forget to sort the reads to ensure that the R1 and R2 reads are in the same order!!!\n\n");
    }
	
	
	/////////////////////////
	//Free variables
	/////////////////////////
	
	free(szInsertDistFile);
	
	for(i=0;i<nr_hap;i++){
		free(Strand[i]);
	}
	free(Strand);
	
	for(j=0;j<4;j++){	
		for(i=0;i<ILL_L;i++){
			free(Ill_Noise_fwd[j][i]);
			free(Ill_Noise_rev[j][i]);
		}
		free(Ill_Noise_fwd[j]);
		free(Ill_Noise_rev[j]);
		free(nucATGC_fwd[j]);
		free(nucATGC_rev[j]);
	}
	for(j=0;j<2;j++){	
		for(i=0;i<ILL_L;i++){
			free(Ill_Noise_Del[j][i]);
			free(Ill_Noise_Ins[j][i]);
		}
		free(Ill_Noise_Del[j]);
		free(Ill_Noise_Ins[j]);
	}	
	free(Ill_Noise_Del);
	free(Ill_Noise_Ins);
	free(Ill_Noise_fwd);
	free(Ill_Noise_rev);
	free(nucATGC_fwd);
	free(nucATGC_rev);


	for(j=0;j<64;j++){	
		free(Ill_motif_fwd[j]);
		free(Ill_motif_rev[j]);
		free(Ill_motif_Del_fwd[j]);
		free(Ill_motif_Del_rev[j]);
		free(Ill_motif_Ins_fwd[j]);
		free(Ill_motif_Ins_rev[j]);
		free(nucATGC_motif_fwd[j]);
		free(nucATGC_motif_rev[j]);
	}	
	free(Ill_motif_fwd);
	free(Ill_motif_rev);	
	free(Ill_motif_Ins_fwd);
	free(Ill_motif_Ins_rev);	
	free(Ill_motif_Del_fwd);
	free(Ill_motif_Del_rev);	
	
	free(nucATGC_motif_fwd);
	free(nucATGC_motif_rev);
	
	for(i=0;i<1000;i++){
		free(dist0[i]);
		free(dist1[i]);
		free(dist2[i]);
		free(dist3[i]);
		free(dist4[i]);
		free(dist5[i]);
		free(dist6[i]);
		free(dist7[i]);
		free(dist8[i]);
		free(dist9[i]);
	}
	
	free(dist0);
	free(dist1);
	free(dist2);
	free(dist3);
	free(dist4);
	free(dist5);
	free(dist6);
	free(dist7);
	free(dist8);
	free(dist9);	
	
	for(i=0;i<InsertDistNrLen;i++){
		free(InDist[i].freq);
		free(InDist[i].len);
	}
	free(InDist);
	
	free(frequencies);
	
	for(i=0;i<myNrThreads;i++){
		gsl_rng_free(r[i]);			
	}
	gsl_rng_free(rng_seed);
	free(r);
	
	for(i=0;i<nr_hap;i++){
		free(haplos->hap_name[i]);
		free(haplos->hap_seq[i]);
		free(len_hap[i]);
	}

	free(haplos->hap_name);
	free(haplos->hap_seq);
	free(haplos);
	free(len_hap);
	free(std_char);
	free(FASTQ);
	free(szParFile);
	
	return 0;
}

